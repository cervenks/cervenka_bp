package Test;

import client.cipher.AdapterDES;
import client.cipher.DES;
import client.main.ConnectionManager;
import client.validation.Validator;

import shared.request.*;
import shared.response.DataResponse;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ThreadLocalRandom;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;

public class DESTest {

    //pro testovani musi byt zaply server
    //testovani porovnava vysledek serveru a lokalni sifry
    //nebo zkousi pres validator

    @Test
    public void testDESJavaLibrary_SameValues_True() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        byte[] keyDes = new byte[8];
        byte[] data = new byte[16];
        ThreadLocalRandom.current().nextBytes(keyDes);
        ThreadLocalRandom.current().nextBytes(data);
        DES des = new DES();
        des.set_key(keyDes);
        byte[] result = des.encrypt(data);

        Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");

        SecretKeySpec key = new SecretKeySpec(keyDes, "DES");
        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] result2 = cipher.doFinal(data);

        assertArrayEquals(result, result2);
    }

    @Test
    public void testDESServer_sameResultAsServer_True() throws IOException {
        byte[] keyDes = new byte[8];
        byte[] data = new byte[16];
        ThreadLocalRandom.current().nextBytes(keyDes);
        ThreadLocalRandom.current().nextBytes(data);

        DES des = new DES();
        des.set_key(keyDes);
        byte[] result = des.encrypt(data);

        ConnectionManager cm = new ConnectionManager();

        IRequest request1 = new KeyUploadRequest(keyDes, KeyType.DES);
        cm.sendRequest(request1);
        IRequest request2 = new CipherRequest(data, CipherAlgorithm.DES, BlockCipherMode.ECB);
        DataResponse response = (DataResponse) cm.sendRequest(request2);

        assertArrayEquals(result, response.getData());
    }

    @Test
    public void DESValidatorTest_ValidatorReturnsTrue_True() throws IOException {
        AdapterDES des = new AdapterDES();
        Validator validator = new Validator(new ConnectionManager("TestDES"));
        assertTrue(validator.validate(des));

    }

}
