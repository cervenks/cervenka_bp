package Test;

import Test.TestAdapters.SignatureAdapter;
import client.main.ConnectionManager;
import client.validation.Validator;
import shared.request.*;
import shared.response.BooleanResponse;
import shared.response.IResponse;
import shared.response.UnsuccessfulResponse;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.*;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SignatureTest {

    //pro testovani musi byt zaply server
    //testovani porovnava vysledek serveru a lokalni sifry
    //nebo zkousi pres validator

    @Test
    public void directSignatureTest_CheckResultViaServerSuccessful_True() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        byte[] data = new byte[65536];
        ThreadLocalRandom.current().nextBytes(data);

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair pair = kpg.generateKeyPair();

        MessageDigest md =  MessageDigest.getInstance("MD5");
        byte[] hash1 = md.digest(data);


        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, pair.getPrivate());
        byte[] hash2 = cipher.doFinal(hash1);

        ConnectionManager cm = new ConnectionManager();
        IRequest request = new KeyUploadRequest(pair.getPublic().getEncoded(), KeyType.RSA_PUBLIC);
        IResponse response = cm.sendRequest(request);
        assert !(response instanceof UnsuccessfulResponse);
        request = new SignatureRequest(data, hash2, SignatureCipherAlgorithm.RSA, HashAlgorithm.MD5);
        response = cm.sendRequest(request);

        assert response instanceof BooleanResponse;
        assertTrue(((BooleanResponse)response).getData());

    }


    @Test
    public void keyGeneratorValidationTest_ValidatorReturnsTrue_True() throws IOException {
        Validator val = new Validator(new ConnectionManager());
        assertTrue(val.validate(new SignatureAdapter(
                HashAlgorithm.MD5, SignatureCipherAlgorithm.RSA, "MD5", "RSA/ECB/NoPadding","RSA")));


    }
}
