package Test;

import Test.TestAdapters.CipherAdapter;
import Test.TestAdapters.RSAAdapter;
import client.main.ConnectionManager;
import client.validation.Validator;
import shared.request.*;
import shared.response.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.*;
import java.util.concurrent.ThreadLocalRandom;

import java.util.Random;

import org.junit.Test;

import static org.junit.Assert.*;


public class CipherTest {

    //pro testovani musi byt zaply server
    //testovani porovnava vysledek serveru a lokalni sifry
    //nebo zkousi pres validator

    @Test
    public void directTestDESCTR_SameResultAsServer_True() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, IOException, InvalidAlgorithmParameterException {

        Random r = new Random();
        byte[] data = new byte[1024];
        byte[] key = new byte[8];
        byte[] vector = new byte[8];
        r.nextBytes(data);
        r.nextBytes(key);
        r.nextBytes(vector);

        SecretKeySpec keySpec = new SecretKeySpec(key, "DES");
        IvParameterSpec iv = new IvParameterSpec(vector);

        Cipher cipher = Cipher.getInstance("DES/CTR/NoPadding");

        cipher.init(Cipher.ENCRYPT_MODE,keySpec, iv);

        byte[] cipher1 = cipher.doFinal(data);

        ConnectionManager cm = new ConnectionManager("TestDES");
        IRequest request = new KeyUploadRequest(key, KeyType.DES);
        assert !(cm.sendRequest(request) instanceof UnsuccessfulResponse);
        request = new CipherRequestWithInitVector(vector, data, CipherAlgorithm.DES, BlockCipherMode.CTR);
        IResponse response = cm.sendRequest(request);

        assert response instanceof DataResponse;

        byte[] cipher2 = ((DataResponse)response).getData();

        assertArrayEquals(cipher1, cipher2);
    }

    @Test
    public void directTestAESCTS_SameResultAsServer_True() throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, IOException, InvalidAlgorithmParameterException {
        byte[] data = new byte[1024];
        byte[] key = new byte[24];
        byte[] vector = new byte[16];
        ThreadLocalRandom.current().nextBytes(data);
        ThreadLocalRandom.current().nextBytes(key);
        ThreadLocalRandom.current().nextBytes(vector);
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        IvParameterSpec iv = new IvParameterSpec(vector);

        Cipher cipher = Cipher.getInstance("AES/CTS/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE,keySpec, iv);

        byte[] cipher1 = cipher.doFinal(data);

        ConnectionManager cm = new ConnectionManager("TestAES");
        IRequest request = new KeyUploadRequest(key, KeyType.AES);
        IResponse response = cm.sendRequest(request);
        assert !(response instanceof UnsuccessfulResponse);
        request = new CipherRequestWithInitVector(vector, data, CipherAlgorithm.AES, BlockCipherMode.CTS);
        response = cm.sendRequest(request);

        assert response instanceof DataResponse;

        byte[] cipher2 = ((DataResponse)response).getData();

        assertArrayEquals(cipher1, cipher2);
    }

    @Test
    public void directTestTripleDESOFB_SameResultAsServer_True() throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, IOException, InvalidAlgorithmParameterException {
        byte[] data = new byte[1024];
        byte[] key = new byte[24];
        byte[] vector = new byte[8];
        ThreadLocalRandom.current().nextBytes(data);
        ThreadLocalRandom.current().nextBytes(key);
        ThreadLocalRandom.current().nextBytes(vector);
        SecretKeySpec keySpec = new SecretKeySpec(key, "DESede");
        IvParameterSpec iv = new IvParameterSpec(vector);

        Cipher cipher = Cipher.getInstance("DESede/OFB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE,keySpec, iv);

        byte[] cipher1 = cipher.doFinal(data);

        ConnectionManager cm = new ConnectionManager("TestTripleDEs");
        IRequest request = new KeyUploadRequest(key, KeyType.TRIPLE_DES);
        IResponse response = cm.sendRequest(request);
        assert !(response instanceof UnsuccessfulResponse);
        request = new CipherRequestWithInitVector(vector, data, CipherAlgorithm.TRIPLE_DES, BlockCipherMode.OFB);
        response = cm.sendRequest(request);

        assert response instanceof DataResponse;

        byte[] cipher2 = ((DataResponse)response).getData();

        assertArrayEquals(cipher1, cipher2);
    }

    @Test
    public void directTestBlowfishPCBC_SameResultAsServer_True() throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, IOException, InvalidAlgorithmParameterException {
        byte[] data = new byte[1024];
        byte[] key = new byte[24];
        byte[] vector = new byte[8];
        ThreadLocalRandom.current().nextBytes(data);
        ThreadLocalRandom.current().nextBytes(key);
        ThreadLocalRandom.current().nextBytes(vector);
        SecretKeySpec keySpec = new SecretKeySpec(key, "Blowfish");
        IvParameterSpec iv = new IvParameterSpec(vector);

        Cipher cipher = Cipher.getInstance("Blowfish/PCBC/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE,keySpec, iv);

        byte[] cipher1 = cipher.doFinal(data);

        ConnectionManager cm = new ConnectionManager("Stepan");
        IRequest request = new KeyUploadRequest(key, KeyType.BLOWFISH);
        IResponse response = cm.sendRequest(request);
        assert !(response instanceof UnsuccessfulResponse);
        request = new CipherRequestWithInitVector(vector, data, CipherAlgorithm.BLOWFISH, BlockCipherMode.PCBC);
        response = cm.sendRequest(request);

        assert response instanceof DataResponse;

        byte[] cipher2 = ((DataResponse)response).getData();

        assertArrayEquals(cipher1, cipher2);
    }

    @Test
    public void directTestRSA_SameResultAsServer_True() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, IOException {
        byte[] data = new byte[30];
        ThreadLocalRandom.current().nextBytes(data);

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair pair = kpg.generateKeyPair();

        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, pair.getPublic());
        byte[] cipher1 = cipher.doFinal(data);

        ConnectionManager cm = new ConnectionManager("RSATest");
        IRequest request = new KeyUploadRequest(pair.getPublic().getEncoded(), KeyType.RSA_PUBLIC);
        IResponse response = cm.sendRequest(request);
        assert !(response instanceof UnsuccessfulResponse);
        request = new CipherRequest(data, CipherAlgorithm.RSA, BlockCipherMode.ECB);
        response = cm.sendRequest(request);

        assert response instanceof DataResponse;

        byte[] cipher2 = ((DataResponse)response).getData();

        assertArrayEquals(cipher1, cipher2);

        cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, pair.getPrivate());
        byte[] data2 = transform(cipher.doFinal(cipher2), 30);

        assertArrayEquals(data, data2);

    }

    private byte[] transform(byte[] data, int length) {
        byte[] rvalue = new byte[length];

        for(int i = 0; i < length; i++) {
            rvalue[i] = data[data.length - length + i];
        }

        return rvalue;
    }

    @Test
    public void testMissingKeyFile_UnsuccessfulResponseWithCorrectReason_True() throws IOException {
        ConnectionManager cm = new ConnectionManager();
        IRequest request = new KeyRemoveRequest(KeyType.TRIPLE_DES);
        IResponse response = cm.sendRequest(request);

        assert !(response instanceof UnsuccessfulResponse);

        request = new CipherRequest(new byte[0], CipherAlgorithm.TRIPLE_DES, BlockCipherMode.ECB);
        response = cm.sendRequest(request);

        assertTrue(response instanceof UnsuccessfulResponse);
        assertSame(((UnsuccessfulResponse) response).getReason(), Reason.INVALID_OR_MISSING_KEY_FILE);

    }



    @Test
    public void testIllegalBlockSize_UnsuccessfulResponseWithCorrectReason_True() throws IOException, NoSuchAlgorithmException {
        byte[] data = new byte[256];
        ThreadLocalRandom.current().nextBytes(data);

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair pair = kpg.generateKeyPair();


        ConnectionManager cm = new ConnectionManager();
        IRequest request = new KeyUploadRequest(pair.getPublic().getEncoded(), KeyType.RSA_PUBLIC);
        IResponse response = cm.sendRequest(request);
        assert !(response instanceof UnsuccessfulResponse);
        request = new CipherRequest(data, CipherAlgorithm.RSA, BlockCipherMode.ECB);
        response = cm.sendRequest(request);

        assertTrue(response instanceof UnsuccessfulResponse);
        assertSame(Reason.ILLEGAL_BLOCK_SIZE, ((UnsuccessfulResponse)response).getReason());
    }

    @Test
    public void testIllegalRSAParameter_UnsuccessfulResponseWithCorrectReason_True() throws IOException, NoSuchAlgorithmException {
        byte[] data = new byte[30];
        byte[] vector = new byte[0];
        ThreadLocalRandom.current().nextBytes(data);

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair pair = kpg.generateKeyPair();

        ConnectionManager cm = new ConnectionManager();
        IRequest request = new KeyUploadRequest(pair.getPublic().getEncoded(), KeyType.RSA_PUBLIC);
        IResponse response = cm.sendRequest(request);
        assert !(response instanceof UnsuccessfulResponse);
        request = new CipherRequestWithInitVector(vector, data, CipherAlgorithm.RSA, BlockCipherMode.OFB);
        response = cm.sendRequest(request);

        assertTrue(response instanceof UnsuccessfulResponse);
        assertSame(Reason.ILLEGAL_RSA_PARAMETER, ((UnsuccessfulResponse)response).getReason());
    }

    @Test
    public void testInvalidKey_UnsuccessfulResponseWithCorrectReason_True() throws IOException {
        byte[] key = new byte[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        byte[] data = new byte[1];
        ThreadLocalRandom.current().nextBytes(data);


        ConnectionManager cm = new ConnectionManager("ERR1");
        assert !(cm.sendRequest(new KeyUploadRequest(key, KeyType.RSA_PUBLIC)) instanceof UnsuccessfulResponse);
        IResponse response = cm.sendRequest(new CipherRequest(data, CipherAlgorithm.RSA, BlockCipherMode.ECB));
        assertTrue(response instanceof UnsuccessfulResponse);
        assertSame(Reason.INVALID_KEY, ((UnsuccessfulResponse)response).getReason());

    }

    @Test
    public void testMissingInitVector_UnsuccessfulResponseWithCorrectReason_True() throws IOException {
        byte[] data = new byte[32];
        byte[] key = new byte[8];
        ThreadLocalRandom.current().nextBytes(data);
        ThreadLocalRandom.current().nextBytes(key);

        ConnectionManager cm = new ConnectionManager();
        assert !(cm.sendRequest(new KeyUploadRequest(key, KeyType.DES)) instanceof UnsuccessfulResponse);
        IResponse response = cm.sendRequest(new CipherRequest(data, CipherAlgorithm.DES, BlockCipherMode.CTS));

        assertTrue(response instanceof UnsuccessfulResponse);
        assertSame(Reason.MISSING_INITIAL_VECTOR, ((UnsuccessfulResponse)response).getReason());
    }

    @Test
    public void testRedundantInitVector_UnsuccessfulResponseWithCorrectReason_True() throws IOException {
        byte[] data = new byte[64];
        byte[] key = new byte[24];
        byte[] vector = new byte[16];
        ThreadLocalRandom.current().nextBytes(data);
        ThreadLocalRandom.current().nextBytes(key);
        ThreadLocalRandom.current().nextBytes(vector);

        ConnectionManager cm = new ConnectionManager();
        assert !(cm.sendRequest(new KeyUploadRequest(key, KeyType.AES)) instanceof UnsuccessfulResponse);
        IResponse response = cm.sendRequest(new CipherRequestWithInitVector(vector, data, CipherAlgorithm.AES, BlockCipherMode.ECB));

        assertTrue(response instanceof UnsuccessfulResponse);
        assertSame(Reason.REDUNDANT_INITIAL_VECTOR, ((UnsuccessfulResponse)response).getReason());



    }


    @Test
    public void validateSymmetricCipher_ValidatorReturnsTrue_True() throws IOException {
        CipherAdapter adapter = new CipherAdapter(CipherAlgorithm.BLOWFISH, BlockCipherMode.CFB, "Blowfish/CFB/NoPadding");
        Validator validator = new Validator(new ConnectionManager());
        boolean bool = validator.validate(adapter);
        assertTrue(bool);
    }

    @Test
    public void validateRSA_ValidatorReturnsTrue_True() throws IOException {
        ConnectionManager cm = new ConnectionManager();
        Validator val = new Validator(cm);
        boolean bool = val.validate(new RSAAdapter());
        assertTrue(bool);
    }








}
