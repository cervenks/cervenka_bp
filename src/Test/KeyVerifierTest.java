package Test;

import Test.TestAdapters.KeyGenerationAdapter;
import client.main.ConnectionManager;
import client.validation.Validator;
import shared.request.KeyVerificationRequest;
import shared.request.KeyVerificationType;
import shared.response.BooleanResponse;
import shared.response.IResponse;

import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class KeyVerifierTest {

    //pro testovani musi byt zaply server
    //testovani porovnava vysledek serveru a lokalni sifry
    //nebo zkousi pres validator

    @Test
    public void directKeyVerificationTest_CheckResultViaServerSuccessful_True() throws IOException, NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(512);
        KeyPair pair = kpg.generateKeyPair();

        ConnectionManager cm = new ConnectionManager();
        IResponse response = cm.sendRequest(
                new KeyVerificationRequest(pair.getPrivate().getEncoded(), pair.getPublic().getEncoded(), KeyVerificationType.RSA));

        assertTrue(response instanceof BooleanResponse);
        assertTrue(((BooleanResponse) response).getData());
    }

    @Test
    public void keyGeneratorValidationTest_ValidatorReturnsTrue_True() throws IOException {
        Validator val = new Validator(new ConnectionManager());
        assertTrue(val.validate(new KeyGenerationAdapter(KeyVerificationType.RSA, "RSA")));

    }
}
