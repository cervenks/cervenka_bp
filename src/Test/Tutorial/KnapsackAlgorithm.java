package Test.Tutorial;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.concurrent.ThreadLocalRandom;

public class KnapsackAlgorithm {


    //maximalni delka vsech hodnot v bytech (hmotnosti, p, q)
    private static int NUMBER_SIZE_IN_BYTES = 2;

    //pocet zavazi v zavadlu
    private static int WEIGHT_COUNT = 8;


    //vahy verejne
    private BigInteger[] privateKeyInt;

    //vahy soukrome
    private BigInteger[] publicKeyInt;

    //klic dle formatu z tutorial2.txt
    private byte[] privateKeyBytes;
    private byte[] publicKeyBytes;

    private BigInteger q;
    private BigInteger p;


    //vygeneruje klice
    public void generateKeys() {
        generatePrivateKey();
        generatePublicKey();

        //prevede klice z bigInteger[] na byte[]
        convertKeys();

    }

    //sestavi soukromy klic
    //nejdrive vlozi q, p a pak zavazi ve formatu byte[]
    public byte[] getPrivateKey() {
        //vytvorime pole, to obalime bufferem
        //pak vkladame hodnoty do bufferu

        byte[] rvalue = new byte[privateKeyBytes.length + 2 * NUMBER_SIZE_IN_BYTES];
        ByteBuffer byteBuffer = ByteBuffer.wrap(rvalue);
        byteBuffer.put(bigIntegerToByteArray(q, NUMBER_SIZE_IN_BYTES));
        byteBuffer.put(bigIntegerToByteArray(p, NUMBER_SIZE_IN_BYTES));
        byteBuffer.put(privateKeyBytes);
        return rvalue;
    }


    //verejny klic je jen mnozina zverejnenych zavazi
    public byte[] getPublicKey() {
        return publicKeyBytes;
    }


    //nastavi soukromy klic z vnejsku
    public void setPrivateKey(byte[] privateKey) {

        //obalime pole bufferem
        ByteBuffer byteBuffer = ByteBuffer.wrap(privateKey);
        byte[] qBytes = new byte[NUMBER_SIZE_IN_BYTES];
        byte[] pBytes = new byte[NUMBER_SIZE_IN_BYTES];

        //pote z bufferu vybirame postupne hodnoty
        byteBuffer.get(qBytes, 0, NUMBER_SIZE_IN_BYTES);
        byteBuffer.get(pBytes, 0, NUMBER_SIZE_IN_BYTES);

        this.q = new BigInteger(qBytes);
        this.p = new BigInteger(pBytes);


        this.privateKeyBytes = new byte[privateKey.length - (2 * NUMBER_SIZE_IN_BYTES)];
        byteBuffer.get(privateKeyBytes, 0, privateKey.length - (2 * NUMBER_SIZE_IN_BYTES));

        //na konci preparsujeme zavazi do bigInteger[]
        this.privateKeyInt = parseByteArrayToBigIntegerArray(this.privateKeyBytes);

    }


    //nastavime verejny klic z vnejsku
    //jedna se jen o jednu hodnotu, takze tu nastavime a prevedeme do big integer
    public void setPublicKey(byte[] publicKey) {
        this.publicKeyBytes = publicKey;
        this.publicKeyInt = parseByteArrayToBigIntegerArray(this.publicKeyBytes);
    }


    //vygeneruje soukromy klic
    private void generatePrivateKey() {

        //nasledujici promenne slouzi k vypoctu delky nejmensiho clenu superrostouci posloupnosti

        //obecne nejvyssi hodnota, kterou musime ulozit do implicitne
        //dvoubytoveho cisla (jinak dle konstany NUMBER_SIZE_IN_BYTES) je
        //soucet vah verejneho klice
        // tj. (pokud budu chtit zasifrovat blok dat 11111111), tak my vyjde prave toto cislo
        //to musi byt mensi nez 2^15 (maximalni hodnota dvojbytoveho cisla,
        // pokud se nechceme zatezovat se zapornymi cisly)

        //hodnoty verejneho klice mohou byt radove az q,
        // za predpokladu, ze pocet zavazadel = 8, musi platit ze 8 * q < 2^15
        // protoze  2^3 * 2^12 = 2 ^15, plati ze velikost q je radove 2^12

        //o vypocet vyse zminene trojky se stara nasledujici vzorecek,
        // protoze trojka je vypocitana jako log2(8)
        //pokud by pocet zavazadel byl napr. 2^6, muselo by platit,
        // ze q bude o 6 radu ve dvojkove soustave mensi nez maximalni soucet vah
        int possibleSum = (int)Math.ceil(Math.log(WEIGHT_COUNT) / Math.log(2));


        //promenna bit length je radova velikost nejmensiho cisla superrostouci posloupnosti
        //predpokladejme tedy, ze q je radove 2^12 (viz vyse)
        //znamena to, ze soucet vah je mensi nez 2^12
        //protoze vaha s nejvetsi hodnotou bude vetsi soucet vsech ostatnich vah dohromady
        //tak staci, kdyz bude radove 2^11, protoze 2 * 2^11 je radove 2^12
        //analogicky takto dojdeme az k nejmensi vaze a zjistime, ze nejmensi vaha
        //musi byt cca o 8 radu mensi nez hodnota q, v nasem pripade tedy 2^4

        //nasledujici vzorecek tedy s implicitnimi hodnotami vrati hodnotu 4
        int bitLength = NUMBER_SIZE_IN_BYTES * 8 - 1 - possibleSum - WEIGHT_COUNT;

        privateKeyInt = new BigInteger[WEIGHT_COUNT];

        BigInteger sum = BigInteger.ZERO;

        //generovani superrostouci posloupnosti probiha tak, ze generujeme cisla od 1 do 2^4
        //tuto hodnotu vzdy pricteme k souctu predchozich cisel
        //kazda hodnota bude tedy o rad (ve dvojkove soustave) vyssi nez predchozi
        for(int i = 0; i < WEIGHT_COUNT; i++) {
            BigInteger random = new BigInteger(bitLength, ThreadLocalRandom.current()).add(BigInteger.ONE);
            BigInteger newNumber = sum.add(random);
            sum = sum.add(newNumber);
            privateKeyInt[i] = newNumber;
        }

        //ten samy proces zopakujeme i pro q
        BigInteger random = new BigInteger(bitLength, ThreadLocalRandom.current()).add(BigInteger.ONE);
        q = sum.add(random);


        //hodnota p musi byt mensi nez q a s q nesoudelna
        //nesoudelnost zjistime tak, ze existuje inverzni p (p^-1) -> metoda modInverse
         while(true) {
            p = new BigInteger(q.bitLength(), ThreadLocalRandom.current());

            if(p.compareTo(q) >= 0) {
                continue;
            }

            try{
                p.modInverse(q);
            } catch (Exception e) {
                continue;
            }

            break;
        }


    }

    private void generatePublicKey() {
        //verejny klic ziskame tranformaci soukromeho klice

        publicKeyInt = new BigInteger[WEIGHT_COUNT];

        for(int i = 0; i < WEIGHT_COUNT; i++) {
            //kazde cislo prenasobime p a modulujeme q
            BigInteger number = privateKeyInt[i].multiply(p);
            number = number.mod(q);
            publicKeyInt[i] = number;
        }
    }

    private byte[] convertKey(BigInteger[] key) {
        //prevod biginteger[] na byte[]

        //bytove pole obalime bufferem

        byte[] rvalue = new byte[key.length * NUMBER_SIZE_IN_BYTES];
        ByteBuffer buffer = ByteBuffer.wrap(rvalue);

        for(int i = 0; i < key.length; i++) {

            //kazde cislo pak prevedeme pomoci nasledujici metody na pole bytu,
            //ktere vlozime do bufferu
            buffer.put(bigIntegerToByteArray(key[i], NUMBER_SIZE_IN_BYTES));
        }

        return buffer.array();
    }

    private byte[] bigIntegerToByteArray(BigInteger bigInteger, int length) {
        //prevod bigIntegeru na pole bytu pomoci metody byteArray
        //musime dat pozor na to, ze big integer nezna pozadovanou velikost pole
        //musi proto vysledek metody toByteArray() prekopirovat do pole spravne delky
        byte[] array = bigInteger.toByteArray();
        byte[] tmp = new byte[length];
        int newPos = tmp.length - array.length;
        System.arraycopy(array, 0, tmp, newPos, array.length);
        return tmp;


    }

    private void convertKeys() {
        //prevedeni klicu
        this.privateKeyBytes = convertKey(privateKeyInt);
        this.publicKeyBytes = convertKey(publicKeyInt);
    }

    public byte[] encrypt(byte[] data) {
        //pro jednoduchost ocekavame, ze delka zpravy bude delitelna poctem vah v zavazadlu
        //tzn. ze velikost bloku vyjde celociselne
        //defaultne je pocet vah 8, takze tato podminka je zarucena vzdy

        int resultSize = data.length * 8; //delka zpravy v bitech
        int blockCount = resultSize / WEIGHT_COUNT; // pocet bloku
        resultSize = blockCount * NUMBER_SIZE_IN_BYTES; // pocet bloku vynasobeno velikosti cisla reprezentujici blok


        //vysledek opet obalime bufferem
        byte[] rvalue = new byte[resultSize];
        ByteBuffer buffer = ByteBuffer.wrap(rvalue);


        for(int i = 0; i < blockCount; i++) {
            //sifrujeme po blocich
            byte[] block = encryptBlock(data, i * WEIGHT_COUNT);

            //vysledek vkladame do bufferu
            buffer.put(block);
        }

        return buffer.array();
    }

    public byte[] decrypt(byte[] data) {

        //nacteme cisla pomoci do pole BigInteger[]

        BigInteger inverseP = p.modInverse(q);
        BigInteger[] numbers = parseByteArrayToBigIntegerArray(data);
        BigInteger[] decryptedNumbers = new BigInteger[numbers.length];


        //cisla zasifrovana pomoci verejneho klice prevedeme na cisla,
        // ktera jdou desifrovat soukromym klicem
        //vzorecek cislo * p^-1 mod q
        for(int i = 0; i < numbers.length; i++) {
            BigInteger current = numbers[i].multiply(inverseP);
            current = current.mod(this.q);
            decryptedNumbers[i] = current;
        }

        int resultSize = data.length / NUMBER_SIZE_IN_BYTES; //pocet cisel ve zprave
        resultSize = resultSize * WEIGHT_COUNT; //delka zpravy v bitech
        resultSize = resultSize / 8; //delka zpravy v bytech
        byte[] rvalue = new byte[resultSize];

        //desifrujeme po blocich, metoda rovnou upravuje bity v byte[]
        for(int i = 0; i < decryptedNumbers.length; i++) {
           decryptBlock(rvalue, i * WEIGHT_COUNT, decryptedNumbers[i]);
        }


        return rvalue;

    }


    //prevod pole bytu na pole big integeru
    private BigInteger[] parseByteArrayToBigIntegerArray(byte[] data) {
        int numberCount = data.length / NUMBER_SIZE_IN_BYTES; //pocet cisel
        BigInteger[] rvalue = new BigInteger[numberCount];
        ByteBuffer buffer = ByteBuffer.wrap(data); //obaleni pole bytu bufferem


        for(int i = 0; i < numberCount; i++) {
            //nacteme byty z velkeho pole reprezentujici jedno cislo
            byte[] current = new byte[NUMBER_SIZE_IN_BYTES];
            buffer.get(current, 0, NUMBER_SIZE_IN_BYTES);

            //cislo prevedeme na big integer
            rvalue[i] = new BigInteger(current);
        }

        return rvalue;
    }

    //sifrovani bloku
    private byte[] encryptBlock(byte[] data, int startPos) {
        BigInteger rvalue = BigInteger.ZERO;
        for(int i = 0; i < WEIGHT_COUNT; i++) {
            //nacitame hodnotu po bitech
            int bit = getBitOfByteArray(data,startPos + i);
            if(bit == 1) {
                //pokud bit = 1, tak pricteme prislusnou vahu
                rvalue = rvalue.add(this.publicKeyInt[i]);
            }
        }

        //nakonec prevedeme big integer na byte[]
        return bigIntegerToByteArray(rvalue, NUMBER_SIZE_IN_BYTES);
    }


    //desifrovani bloku
    private void decryptBlock(byte[] rvalue, int startPos, BigInteger number) {

        //potrebujeme ziskat cisla, jejichz soucet da dohromady cislo ulozene v promenne number
        for(int i = this.privateKeyInt.length - 1; i >= 0; i--) {
            //budeme porovnavat od nejvyssich vah
            if(number.compareTo(this.privateKeyInt[i]) >= 0) {
                //pokud je vaha mensi nez cislo, tak je vaha pouzita a nastavime prislusny bit
                setBitOfByteArray(rvalue, startPos + i, 1);

                //vaha uz je zapoctena, proto ji odecteme od cisla
                number = number.subtract(this.privateKeyInt[i]);
            }
        }
    }


    //zjisti konkretni bit z byte[]
    private int getBitOfByteArray(byte[] input, int position) {
        int a = position / 8;
        int b = position % 8;

        byte temp = (byte)((input[a] << b) & -128);

        if(temp == -128) {
            return 1;
        }

        if(temp == 0) {
            return 0;
        }

        return -1;
    }


    //nastavi konkretni byt byte[]
    public static void setBitOfByteArray(byte[] input, int position, int bit) {

        int a = position / 8;
        int b = 7 - position % 8;

        if(bit == 0) {
            input[a] &= ~(1 << b);
        } else if (bit == 1) {
            input[a] |= 1 << b;
        }
    }

}
