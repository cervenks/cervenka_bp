package Test.Tutorial;

import client.main.ConnectionManager;
import client.structures.IAsymmetricCipher;
import client.structures.IKeyGenerator;
import client.validation.Validator;
import shared.request.*;
import shared.response.BooleanResponse;
import shared.response.DataResponse;
import shared.response.IResponse;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;


public class KnapsackTest {

    @Test
    public void localTest() {

        //100x zkusime vygenerovat klice, zasifrovat a desifrovat
        //pokud se vysledek rovna puvodni zprave, tak test prosel
       for (int i = 0; i < 100; i++) {
           KnapsackAlgorithm knapsackSimple = new KnapsackAlgorithm();
           knapsackSimple.generateKeys();

            byte[] data = new byte[ThreadLocalRandom.current().nextInt(100)];
            ThreadLocalRandom.current().nextBytes(data);
            byte[] cipher = knapsackSimple.encrypt(data);
            byte[] plain = knapsackSimple.decrypt(cipher);
            assertArrayEquals(data, plain);
       }



    }



    @Test
    public void serverTest() throws IOException {
        //pripojime se na server
        ConnectionManager cm = new ConnectionManager("Knapsack");

        //zalozime tridu knapsack a vygenerujeme klice
        KnapsackAlgorithm ka = new KnapsackAlgorithm();
        ka.generateKeys();

        //posleme request na nahrani klice
        cm.sendRequest(new KeyUploadRequest(ka.getPublicKey(), KeyType.KNAPSACK_PUBLIC));

        //overime si, ze algoritmus na serveru dokaze sifrovat a desifrovat s nasimi klici
        IResponse verResponse = cm.sendRequest(new KeyVerificationRequest(ka.getPrivateKey(), ka.getPublicKey(), KeyVerificationType.KNAPSACK));
        assertTrue(verResponse instanceof BooleanResponse);
        assertTrue(((BooleanResponse)verResponse).getData());

        //vygenerujeme nahodna data
        byte[] data = new byte[1000];
        ThreadLocalRandom.current().nextBytes(data);


        //zasifrujeme na serveru i lokalne
        IResponse response = cm.sendRequest(new CipherRequest(data, CipherAlgorithm.KNAPSACK, BlockCipherMode.ECB));
        assertTrue(response instanceof DataResponse);

        byte[] serverResult = ((DataResponse)response).getData();
        byte[] localResult = ka.encrypt(data);


        //porovname vysledek
        assertArrayEquals(serverResult, localResult);


    }


    @Test
    public void validatorEncryptionTest() throws IOException {

        //vyzkousime sifru na validatoru
        IAsymmetricCipher cipher = new KnapsackAdapterCipher();
        Validator val = new Validator(new ConnectionManager("Tutorial1"));

        boolean bool = val.validate(cipher);
        assertTrue(bool);



    }

    @Test
    public void validatorKeyGenerationTest() throws IOException {
        IKeyGenerator generator = new KnapsackAdapterKeyGeneration();
        ConnectionManager cm = new ConnectionManager("Tutorial2");

        //vyzkousime sifru na validatoru

        Validator validator = new Validator(cm);
        boolean bool = validator.validate(generator);
        assertTrue(bool);
    }


}
