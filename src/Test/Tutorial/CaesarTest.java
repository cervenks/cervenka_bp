package Test.Tutorial;

import client.main.ConnectionManager;
import client.validation.Validator;
import shared.request.*;
import shared.response.DataResponse;
import shared.response.IResponse;
import shared.response.SuccessfulResponse;

import java.io.IOException;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;


public class CaesarTest {

    @Test
    public void simpleCaesarTest() {
        //jednoduche overeni na primitivnich datech

        CaesarSimple cipher = new CaesarSimple(1);
        byte[] encrypted = cipher.encrypt("AHOJZZ123abc".getBytes());
        assertTrue("BIPKAA123abc".equals(new String(encrypted)));
    }

    @Test
    public void serverVerification() throws IOException {
        //overeni pres server

        byte[] data = "DLOUHA_ZPRAVA_PRO_OVERENI_NA_SERVER_totobysenemeloprevest".getBytes();
        byte[] key = new byte[]{12};

        CaesarSimple cipher = new CaesarSimple(key[0]);

        //nejdrive na server ulozime nas klic
        ConnectionManager cm = new ConnectionManager();
        IResponse response = cm.sendRequest(new KeyUploadRequest(key, KeyType.CAESAR));
        assertTrue(response instanceof SuccessfulResponse);


        //pote posleme request na zasifrovani
        response = cm.sendRequest(new CipherRequest(data, CipherAlgorithm.CAESAR, BlockCipherMode.ECB));
        assertTrue(response instanceof DataResponse);

        //vypiseme jmeno a oba vysledky
        System.out.println(cm.getNickname());
        byte[] serverResult = ((DataResponse)response).getData();
        System.out.println(new String(serverResult));
        byte[] localResult = cipher.encrypt(data);
        System.out.println(new String(localResult));


        //vysledky porovname
        assertArrayEquals(serverResult, localResult);


    }

    @Test
    public void validatorTest() throws IOException {

        //vyzkousime sifru na validatoru
        Validator val = new Validator(new ConnectionManager());

        assertTrue(val.validate(new CaesarAdapter()));
    }


}
