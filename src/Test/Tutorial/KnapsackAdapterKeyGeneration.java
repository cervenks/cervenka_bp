package Test.Tutorial;

import client.structures.IKeyGenerator;
import shared.request.KeyType;
import shared.request.KeyVerificationType;

public class KnapsackAdapterKeyGeneration implements IKeyGenerator {

    //adapter pro generovani klicu pro zavazadlovy algoritmus

    //prislusne metody delegujeme na trid KnapsackAlgorithm, nebo vracime metadata

    private KnapsackAlgorithm ka;

    public KnapsackAdapterKeyGeneration() {
        this.ka = new KnapsackAlgorithm();
    }

    @Override
    public KeyVerificationType getKeyType() {
        return KeyVerificationType.KNAPSACK;
    }

    @Override
    public void generate() {
        ka.generateKeys();
    }

    @Override
    public byte[] getPrivateKey() {
        return ka.getPrivateKey();
    }

    @Override
    public byte[] getPublicKey() {
        return ka.getPublicKey();
    }
}
