package Test.Tutorial;

import client.structures.ISymmetricCipher;
import shared.request.BlockCipherMode;
import shared.request.CipherAlgorithm;

public class CaesarAdapter implements ISymmetricCipher {

    //adapter pro validator

    //implementujeme rozhrani, bud vracime metadata nebo deleguje na tridu s algoritmem

    private CaesarSimple caesarSimple;

    public CaesarAdapter() {
        this.caesarSimple = new CaesarSimple(0);
    }

    @Override
    public CipherAlgorithm getCipherAlgorithm() {
        return CipherAlgorithm.CAESAR;
    }

    @Override
    public BlockCipherMode getBlockCipherMode() {
        return BlockCipherMode.ECB;
    }

    @Override
    public byte[] encrypt(byte[] data) {
        return caesarSimple.encrypt(data);
    }

    @Override
    public void setKey(byte[] key) {
        this.caesarSimple.setOffset(key[0]);

    }

    @Override
    public void setInitVector(byte[] vector) {

    }
}
