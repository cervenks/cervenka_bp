package Test.Tutorial;

import client.structures.IAsymmetricCipher;
import shared.request.CipherAlgorithm;

public class KnapsackAdapterCipher implements IAsymmetricCipher {
    //adapter pro sifrovani zavazadlovym algoritmem

    //prislusne metody delegujeme na trid KnapsackAlgorithm, nebo vracime metadata

    private KnapsackAlgorithm ka;

    public KnapsackAdapterCipher() {
        ka = new KnapsackAlgorithm();
    }

    @Override
    public CipherAlgorithm getCipherAlgorithm() {
        return CipherAlgorithm.KNAPSACK;
    }

    @Override
    public byte[] encryptWithPublicKey(byte[] data) {
        return this.ka.encrypt(data);
    }

    @Override
    public byte[] decryptWithPrivateKey(byte[] data) {
        return this.ka.decrypt(data);
    }

    @Override
    public void setPublicKey(byte[] key) {
        this.ka.setPublicKey(key);
    }

    @Override
    public void setPrivateKey(byte[] key) {
        this.ka.setPrivateKey(key);
    }
}
