package Test.Tutorial;


public class CaesarSimple {

    private int offset;

    public CaesarSimple(int offset) {
        this.offset = offset;
    }


    //zasifruje zpravu, zprava je sifrovana po znacich
    public byte[] encrypt(byte[] message) {
        byte[] rvalue = new byte[message.length];

        for (int i = 0; i < message.length; i++) {
            char c = moveChar((char)message[i]);
            rvalue[i] = (byte)c;
        }

        return rvalue;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }


    //zasifruje znak
    private char moveChar(char c) {
        if (!(between('A', 'Z', c))) { //budeme posouvat pismena pouze z rozsahu A-Z
            return c;
        }

        int originalPos = c - 'A'; //pozice v abecede
        int newPos = (originalPos + offset) % 26; //nova pozice v abecede

        return (char) ('A' + newPos);
    }

    private boolean between(char min, char max, char c) {
        return c >= min && c <= max;
    }



}

