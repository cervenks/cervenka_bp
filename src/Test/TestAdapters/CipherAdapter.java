package Test.TestAdapters;

import client.structures.ISymmetricCipher;
import shared.request.BlockCipherMode;
import shared.request.CipherAlgorithm;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.X509EncodedKeySpec;

public class CipherAdapter implements ISymmetricCipher {


    //adapter pro validaci symetrickeho sifrovani
    //delegovani metod na tridu Cipher

    private CipherAlgorithm alg;
    private BlockCipherMode mode;
    private String initString;
    private byte[] key;
    private Key keySpec;
    private byte[] vector;

    public CipherAdapter(CipherAlgorithm alg, BlockCipherMode mode, String initString) {
        this.alg = alg;
        this.mode = mode;
        this.initString = initString;
    }

    @Override
    public CipherAlgorithm getCipherAlgorithm() {
        return alg;
    }

    @Override
    public BlockCipherMode getBlockCipherMode() {
        return mode;
    }

    @Override
    public byte[] encrypt(byte[] data) {
        return mode == BlockCipherMode.ECB ?  encryptWOVector(data) : encryptWithVector(data);
    }

    private byte[] encryptWOVector(byte[] data) {
        try {
            Cipher cipher  = Cipher.getInstance(initString);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
            return cipher.doFinal(data);
        } catch (NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException | NoSuchPaddingException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    private byte[] encryptWithVector(byte[] data) {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(initString);
            IvParameterSpec iv = new IvParameterSpec(vector);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);
            return cipher.doFinal(data);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }


    @Override
    public void setInitVector(byte[] vector) {
        this.vector = vector;
    }

    @Override
    public void setKey(byte[] key) {
        this.key = key;
        this.keySpec = new SecretKeySpec(key, alg.toString());
    }

    public byte[] getKey() {
        return key;
    }
}
