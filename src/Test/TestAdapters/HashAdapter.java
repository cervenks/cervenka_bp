package Test.TestAdapters;

import client.structures.IHash;
import shared.request.HashAlgorithm;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashAdapter implements IHash {

    private HashAlgorithm alg;
    private String initString;

    //adapter pro validaci hashovani
    //delegovani metod na MessageDigest


    public HashAdapter(HashAlgorithm alg, String initString) {
        this.alg = alg;
        this.initString = initString;
    }

    @Override
    public HashAlgorithm getHashAlgorithm() {
        return  this.alg;
    }

    @Override
    public byte[] hash(byte[] data) {
        try {
            return _hash(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }


    private byte[] _hash(byte[] data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(initString);
        return md.digest(data);
    }
}
