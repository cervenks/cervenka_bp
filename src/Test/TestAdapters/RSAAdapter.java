package Test.TestAdapters;

import client.structures.IAsymmetricCipher;
import client.structures.ISymmetricCipher;
import shared.request.BlockCipherMode;
import shared.request.CipherAlgorithm;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSAAdapter implements IAsymmetricCipher {

    //adapter pro validaci RSA
    //delegovani metod na Cipher


    private X509EncodedKeySpec publicKeySpec;
    private PKCS8EncodedKeySpec privateKeySpec;

    @Override
    public byte[] encryptWithPublicKey(byte[] data) {
        try {
            return _encrypt(data);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }

    private byte[] _encrypt(byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        assert publicKeySpec != null;
        cipher.init(Cipher.ENCRYPT_MODE, KeyFactory.getInstance("RSA").generatePublic(publicKeySpec));
        return cipher.doFinal(data);
    }

    @Override
    public byte[] decryptWithPrivateKey(byte[] data) {
        try {
            return _decrypt(data);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }

    private byte[] _decrypt(byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        assert publicKeySpec != null;
        cipher.init(Cipher.DECRYPT_MODE, KeyFactory.getInstance("RSA").generatePrivate(privateKeySpec));
        return cipher.doFinal(data);
    }

    @Override
    public void setPublicKey(byte[] key) {
        this.publicKeySpec = new X509EncodedKeySpec(key);
    }

    @Override
    public void setPrivateKey(byte[] key) {
        this.privateKeySpec = new PKCS8EncodedKeySpec(key);
    }

    @Override
    public CipherAlgorithm getCipherAlgorithm() {
        return CipherAlgorithm.RSA;
    }

}
