package Test.TestAdapters;

import client.structures.ISignature;
import shared.request.HashAlgorithm;
import shared.request.SignatureCipherAlgorithm;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class SignatureAdapter implements ISignature {

    //adapter pro validaci digitalniho podpisu
    //delegovani metod na Cipher a MessageDigest


    private HashAlgorithm hashAlgorithm;
    private SignatureCipherAlgorithm signatureCipherAlgorithm;
    private String hashInitString;
    private String cipherInitString;
    private String keyFactoryString;

    private PKCS8EncodedKeySpec privateKeySpec;

    public SignatureAdapter(HashAlgorithm hashAlgorithm, SignatureCipherAlgorithm signatureCipherAlgorithm, String hashInitString, String cipherInitString, String keyFactoryString) {
        this.hashAlgorithm = hashAlgorithm;
        this.signatureCipherAlgorithm = signatureCipherAlgorithm;
        this.hashInitString = hashInitString;
        this.cipherInitString = cipherInitString;
        this.keyFactoryString = keyFactoryString;
    }

    @Override
    public HashAlgorithm getHashAlgorithm() {
        return hashAlgorithm;
    }

    @Override
    public SignatureCipherAlgorithm getCipherAlgorithm() {
        return signatureCipherAlgorithm;
    }

    @Override
    public byte[] sign(byte[] data) {
        try {
            return _sign(data);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidKeySpecException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    private byte[] _sign(byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeySpecException, InvalidKeyException {
        MessageDigest md =  MessageDigest.getInstance(hashInitString);
        byte[] hash1 = md.digest(data);


        Cipher cipher = Cipher.getInstance(cipherInitString);
        cipher.init(Cipher.DECRYPT_MODE, KeyFactory.getInstance(keyFactoryString).generatePrivate(privateKeySpec));
        return cipher.doFinal(hash1);
    }

    @Override
    public void setPrivateKey(byte[] key) {
        this.privateKeySpec = new PKCS8EncodedKeySpec(key);
    }
}
