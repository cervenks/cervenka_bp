package Test.TestAdapters;

import client.structures.IKeyGenerator;
import shared.request.KeyType;
import shared.request.KeyVerificationType;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

public class KeyGenerationAdapter implements IKeyGenerator {

    //adapter pro validaci generovani klicu
    //delegovani metod na KeyPairGenerator


    private KeyVerificationType keyVerificationType;
    private KeyPair keyPair;
    private String initString;

    public KeyGenerationAdapter(KeyVerificationType keyVerificationType, String initString) {
        this.initString = initString;
        this.keyVerificationType = keyVerificationType;
    }

    @Override
    public KeyVerificationType getKeyType() {
        return this.keyVerificationType;
    }

    @Override
    public void generate() {
        KeyPairGenerator kpg;
        try {
            kpg = KeyPairGenerator.getInstance(initString);
            kpg.initialize(1234);
            this.keyPair = kpg.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    @Override
    public byte[] getPrivateKey() {
        assert keyPair != null;
        return keyPair.getPrivate().getEncoded();
    }

    @Override
    public byte[] getPublicKey() {
        assert keyPair != null;
        return keyPair.getPublic().getEncoded();
    }
}
