package Test;

import Test.TestAdapters.HashAdapter;
import client.main.ConnectionManager;
import client.validation.Validator;

import shared.request.HashAlgorithm;
import shared.request.HashRequest;
import shared.response.DataResponse;
import shared.response.IResponse;


import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;


import org.junit.Test;

public class HashTest {


    //pro testovani musi byt zaply server
    //testovani porovnava vysledek serveru a lokalni sifry
    //nebo zkousi pres validator

    @Test
    public void directTestMd2_SameResultAsServer_True() throws NoSuchAlgorithmException, IOException {
        byte[] data = new byte[65536];
        ThreadLocalRandom.current().nextBytes(data);

        MessageDigest md =  MessageDigest.getInstance("MD2");
        byte[] hash1 = md.digest(data);

        ConnectionManager cm = new ConnectionManager();
         IResponse response = cm.sendRequest(new HashRequest(data, HashAlgorithm.MD2));
        assert response instanceof DataResponse;

        byte[] hash2 = ((DataResponse)response).getData();

        assertArrayEquals(hash1, hash2);
    }

    @Test
    public void directTestMd5_SameResultAsServer_True() throws NoSuchAlgorithmException, IOException {
        byte[] data = new byte[65536];
        ThreadLocalRandom.current().nextBytes(data);

        MessageDigest md =  MessageDigest.getInstance("MD5");
        byte[] hash1 = md.digest(data);

        ConnectionManager cm = new ConnectionManager();
         IResponse response = cm.sendRequest(new HashRequest(data, HashAlgorithm.MD5));
        assert response instanceof DataResponse;

        byte[] hash2 = ((DataResponse)response).getData();

        assertArrayEquals(hash1, hash2);
    }

    @Test
    public  void directTestSha256_SameResultAsServer_True() throws NoSuchAlgorithmException, IOException {
        byte[] data = new byte[65536];
        ThreadLocalRandom.current().nextBytes(data);

        MessageDigest md =  MessageDigest.getInstance("SHA-256");
        byte[] hash1 = md.digest(data);

        ConnectionManager cm = new ConnectionManager();
         IResponse response = cm.sendRequest(new HashRequest(data, HashAlgorithm.SHA256));
        assert response instanceof DataResponse;

        byte[] hash2 = ((DataResponse)response).getData();

        assertArrayEquals(hash1, hash2);
    }

    @Test
    public void directTestSha1_SameResultAsServer_True() throws NoSuchAlgorithmException, IOException {
        byte[] data = new byte[65536];
        ThreadLocalRandom.current().nextBytes(data);

        MessageDigest md =  MessageDigest.getInstance("SHA1");
        byte[] hash1 = md.digest(data);

        ConnectionManager cm = new ConnectionManager();
         IResponse response = cm.sendRequest(new HashRequest(data, HashAlgorithm.SHA1));
        assert response instanceof DataResponse;

        byte[] hash2 = ((DataResponse)response).getData();

        assertArrayEquals(hash1, hash2);
    }

    @Test
    public void directTestSHA384_SameResultAsServer_True() throws NoSuchAlgorithmException, IOException {
        byte[] data = new byte[65536];
        ThreadLocalRandom.current().nextBytes(data);

        MessageDigest md =  MessageDigest.getInstance("SHA-384");
        byte[] hash1 = md.digest(data);

        ConnectionManager cm = new ConnectionManager();
         IResponse response = cm.sendRequest(new HashRequest(data, HashAlgorithm.SHA384));
        assert response instanceof DataResponse;

        byte[] hash2 = ((DataResponse)response).getData();

        assertArrayEquals(hash1, hash2);
    }

    @Test
    public void directTestSHA512_SameResultAsServer_True() throws NoSuchAlgorithmException, IOException {
        byte[] data = new byte[65536];
        ThreadLocalRandom.current().nextBytes(data);

        MessageDigest md =  MessageDigest.getInstance("SHA-512");
        byte[] hash1 = md.digest(data);

        ConnectionManager cm = new ConnectionManager();
         IResponse response = cm.sendRequest(new HashRequest(data, HashAlgorithm.SHA512));
        assert response instanceof DataResponse;

        byte[] hash2 = ((DataResponse)response).getData();

        assertArrayEquals(hash1, hash2);
    }

    @Test
    public void validateHash_ValidatorReturnsTrue_True() throws IOException {
        ConnectionManager cm = new ConnectionManager();
        Validator val = new Validator(cm);
        boolean bool = val.validate(new HashAdapter(HashAlgorithm.MD5, "MD5"));
        assertTrue(bool);
    }
}
