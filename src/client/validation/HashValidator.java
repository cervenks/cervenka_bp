package client.validation;

import client.main.ConnectionManager;
import client.structures.IHash;
import shared.request.HashRequest;
import shared.response.DataResponse;
import shared.response.IResponse;
import shared.response.SuccessfulResponse;

import javax.xml.bind.DatatypeConverter;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class HashValidator {


    //pracujeme se dvema hlavnimi objekty -> connection manager a hash
    //na ty delegujeme hashovani
    private ConnectionManager cm;
    private IHash hash;

    //toto urcuje, s jakymy daty budeme zkouset algoritmus
    //zacneme na FIRST_BYTE_SIZE, ktery vzdy nasobime MULTIPLIER,
    // dokud delka nepresahne MAX_BYTES_SIZE (viz metoda check())
    private static final int MAX_BYTES_SIZE = 10000;
    private static final int FIRST_BYTE_SIZE = 10;
    private static final int MULTIPLIER = 2;


    //protected, protoze ho chceme volat pouze pres validator
    protected HashValidator(ConnectionManager cm, IHash hash) {
        this.cm = cm;
        this.hash = hash;
    }

    //neposilame klic, takze hned do metody check
    public boolean validate() {
        return check();
    }


    //vysvetleno v asymmetricCipherValidator v metode check
    private boolean check() {
        int current_bytes_size = FIRST_BYTE_SIZE;

        while (current_bytes_size <= MAX_BYTES_SIZE) {
            if (!_check(current_bytes_size)) {
                return false;
            }
            current_bytes_size *= MULTIPLIER;
        }

        return true;
    }


    private boolean _check(int size) {
        //vygenerujeme nahodna data o velikosti size
        byte[] data = new byte[size];
        ThreadLocalRandom.current().nextBytes(data);

        //odesleme request prijmeme response
        IResponse response = cm.sendRequest(new HashRequest(data, this.hash.getHashAlgorithm()));

        //dle protokolu, hash prakticky nemuze vratit unsuccessful response
        assert response instanceof DataResponse;


        //ziskani hodnot ze serveru a lokalni sifry
        byte[] serverResult = ((DataResponse) response).getData();
        byte[] localResult = hash.hash(data);


        //porovnani
        if (Arrays.equals(serverResult, localResult)) {
            System.out.println("Data " + size + " bytes long successfully checked.");
            return true;
        } else {
            System.err.println("No success. Data " + size + " bytes long.");
            System.err.println("Data: " + DatatypeConverter.printHexBinary(data));
            System.err.println("Expected: " + DatatypeConverter.printHexBinary(serverResult));
            System.err.println("Actual: " + DatatypeConverter.printHexBinary(localResult));
            return false;
        }
    }
}
