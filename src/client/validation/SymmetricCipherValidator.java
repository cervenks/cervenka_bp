package client.validation;

import client.main.ConnectionManager;
import client.structures.ISymmetricCipher;
import shared.request.*;
import shared.response.DataResponse;
import shared.response.IResponse;
import shared.response.SuccessfulResponse;
import shared.response.UnsuccessfulResponse;

import javax.xml.bind.DatatypeConverter;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class SymmetricCipherValidator {

    //pracujeme se dvema hlavnimi objekty -> connection manager a cipher
    //na ty deleguje sifrovani
    private ConnectionManager connectionManager;
    private ISymmetricCipher cipher;

    //sem ulozime klic
    private byte[] key;


    //toto urcuje, s jakymy daty budeme zkouset algoritmus
    //zacneme na FIRST_BYTE_SIZE, ktery vzdy nasobime MULTIPLIER,
    // dokud delka nepresahne MAX_BYTES_SIZE (viz metoda check())
    private static final int MAX_BLOCK_SIZE = 128;
    private static final int FIRST_BLOCK_SIZE = 1;
    private static final int MULTIPLIER = 2;

    protected SymmetricCipherValidator(ConnectionManager connectionManager, ISymmetricCipher cipher) {
        this.connectionManager = connectionManager;
        this.cipher = cipher;

        //vygenerujeme a ulozime klice v ramci konstruktoru
        this.key = KeyGenerator.generateSymmetricKey(cipher.getCipherAlgorithm());
        assert key != null;
        this.cipher.setKey(key);
    }


    //viz assymetric cipher validator
    boolean validate() {
        if(!uploadKey()) {
            return false;
        }

        return check();
    }

    //viz asymmetric cipher validator
   private boolean uploadKey() {

        IRequest request = new KeyUploadRequest(key, cipher.getCipherAlgorithm().getServerKey());
        IResponse response = connectionManager.sendRequest(request);

        if(response instanceof UnsuccessfulResponse) {
            System.err.println("No success. Reason: " + ((UnsuccessfulResponse)response).getReason());
            return false;
        }

        return true;
    }

    //viz asymmetric cipher validator
    private boolean check() {
        int current_block_size = FIRST_BLOCK_SIZE;

        while(current_block_size <= MAX_BLOCK_SIZE) {
            boolean rvalue;

            //pouze podle blokoveho modu bud budeme pouzivat inicializacni vektor nebo ne
            if(cipher.getBlockCipherMode() == BlockCipherMode.ECB) {
                rvalue = checkWOVector(current_block_size);
            } else {
                rvalue = checkWithVector(current_block_size);
            }

            if(!rvalue) {
                return false;
            }

            current_block_size *= MULTIPLIER;
        }

        return true;

    }

    //kontrola bez inicializacni vektoru
   private boolean checkWOVector(int blockCount) {
        //vygenerujeme data dlouha podle blocku a naplnime nahodnymi daty
        byte[] data = new byte[blockCount * cipher.getCipherAlgorithm().getBlockSize() / 8];
        ThreadLocalRandom.current().nextBytes(data);


        //odesleme request, prijmeme response
        IRequest request = new CipherRequest(data, cipher.getCipherAlgorithm(), cipher.getBlockCipherMode());
        IResponse response = connectionManager.sendRequest(request);


        //test na unsuccessful response
        if(response instanceof UnsuccessfulResponse) {
            System.err.println("No success. Reason: " +  ((UnsuccessfulResponse)response).getReason());
            return false;
        }

        //dle protokolu
        assert response instanceof DataResponse;


        //pripravime vysledky na porovnani
        byte[] serverResult = ((DataResponse)response).getData();
        byte[] localResult = cipher.encrypt(data);


        //porovnani
        if(Arrays.equals(serverResult, localResult)) {
            System.out.println("Data " + blockCount + " blocks long successfully checked.");
            return true;
        } else {
            System.err.println("No success. Data " + blockCount + " blocks long.");
            System.err.println("Data: " + DatatypeConverter.printHexBinary(data));
            System.err.println("Expected: " + DatatypeConverter.printHexBinary(serverResult));
            System.err.println("Actual: " + DatatypeConverter.printHexBinary(localResult));
            return false;
        }
    }

    private boolean checkWithVector(int blockCount) {

        //vygenerujeme data
        byte[] data = new byte[blockCount * cipher.getCipherAlgorithm().getBlockSize() / 8];
        ThreadLocalRandom.current().nextBytes(data);


        //vygenerujeme vektor
        byte[] vector = new byte[cipher.getCipherAlgorithm().getBlockSize() / 8];
        ThreadLocalRandom.current().nextBytes(vector);

        //odesleme request, prijmeme response
        IRequest request = new CipherRequestWithInitVector(vector, data, cipher.getCipherAlgorithm(), cipher.getBlockCipherMode());
        IResponse response = connectionManager.sendRequest(request);


        //test na unsuccessful repsonse
        if(response instanceof UnsuccessfulResponse) {
            System.err.println("No success. Reason: " +  ((UnsuccessfulResponse)response).getReason());
            return false;
        }

        //dle protokolu
        assert response instanceof DataResponse;

        //priprava vysledku na porovnani
        byte[] serverResult = ((DataResponse)response).getData();
        cipher.setInitVector(vector);
        byte[] localResult = cipher.encrypt(data);


        //porovnani
        if(Arrays.equals(serverResult, localResult)) {
            System.out.println("Data " + blockCount + " blocks long successfully checked.");
            return true;
        } else {
            System.err.println("No success. Data " + blockCount + " blocks long.");
            System.err.println("Data: " + DatatypeConverter.printHexBinary(data));
            System.err.println("Init. Vector: " + DatatypeConverter.printHexBinary(vector));
            System.err.println("Expected: " + DatatypeConverter.printHexBinary(serverResult));
            System.err.println("Actual: " + DatatypeConverter.printHexBinary(localResult));
            return false;
        }
    }
}
