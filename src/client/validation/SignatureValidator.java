package client.validation;

import client.main.ConnectionManager;
import client.structures.ISignature;
import shared.request.*;
import shared.response.BooleanResponse;
import shared.response.IResponse;
import shared.response.SuccessfulResponse;
import shared.response.UnsuccessfulResponse;

import javax.xml.bind.DatatypeConverter;
import java.security.KeyPair;
import java.util.concurrent.ThreadLocalRandom;

public class SignatureValidator {


    //pracujeme se dvema hlavnimi objekty -> connection manager a signature
    //signature podepise zpravu
    //connection manager pouze overuje
    private ConnectionManager connectionManager;
    private ISignature signature;

     //sem prijdou klice
    private byte[] privateKey;
    private byte[] publicKey;


    //toto urcuje, s jakymy daty budeme zkouset algoritmus
    //zacneme na FIRST_BYTE_SIZE, ktery vzdy nasobime MULTIPLIER,
    //dokud delka nepresahne MAX_BYTES_SIZE (viz metoda check())
    private static final int MAX_BYTES_SIZE = 10000;
    private static final int FIRST_BYTE_SIZE = 10;
    private static final int MULTIPLIER = 2;

    protected SignatureValidator(ConnectionManager cm, ISignature signature) {
        //ulozeni objektu
        this.connectionManager = cm;
        this.signature = signature;

        //vygenerovani klicu
        byte[][] keys = KeyGenerator.getSignatureKeys(this.signature.getCipherAlgorithm());
        assert keys != null;

        //ulozeni klicu
        this.privateKey = keys[0];
        this.publicKey = keys[1];

        //nastavime soukromy klic pro podepisovani
        this.signature.setPrivateKey(this.privateKey);
    }

    //viz assymetric cipher validator metoda validate
    public boolean validate() {
        if(!uploadKey()) {
            return false;
        }

        return check();
    }

    //viz asymmetric cipher validator metoda upload key
    private boolean uploadKey() {
        IRequest request = new KeyUploadRequest(this.publicKey, signature.getCipherAlgorithm().getServerKey());
        IResponse response = connectionManager.sendRequest(request);

        if(response instanceof UnsuccessfulResponse) {
            System.err.println("No success. Reason: " + ((UnsuccessfulResponse)response).getReason());
            return false;
        }

        return true;
    }

    //viz asymmetric cipher validator metoda check
    private boolean check() {
        int current_bytes_size = FIRST_BYTE_SIZE;

        while (current_bytes_size <= MAX_BYTES_SIZE) {
            if (!_check(current_bytes_size)) {
                return false;
            }
            current_bytes_size *= MULTIPLIER;
        }

        return true;
    }

    private boolean _check(int size) {
        //vygenerovani nahodne prosteho textu delky x
        byte[] data = new byte[size];
        ThreadLocalRandom.current().nextBytes(data);

        //podepsani prosteho textu
        byte[] hash = this.signature.sign(data);

        //overeni podpisu na serveru
        IResponse response = connectionManager.sendRequest(new SignatureRequest(data, hash, this.signature.getCipherAlgorithm(), this.signature.getHashAlgorithm()));


        //pokud se nepovedlo bude unsuccessfulresponse
        if(response instanceof UnsuccessfulResponse) {
            System.err.println("No success. Reason: " +  ((UnsuccessfulResponse)response).getReason());
            return false;
        }

        //dle protokolu
        assert response instanceof BooleanResponse;


        //kontrola navratove hodnoty ze serveru
        if(((BooleanResponse)response).getData()) {
            System.out.println("Data " + size + " bytes long successfully signed and verified.");
            return true;
        } else {
            System.err.println("Cannot verify data " + size + " bytes long.");
            System.err.println("Data = " + DatatypeConverter.printHexBinary(data));
            System.err.println("Hash = " + DatatypeConverter.printHexBinary(hash));
            return false;
        }
    }
}
