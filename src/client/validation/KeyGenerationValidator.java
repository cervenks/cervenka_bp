package client.validation;

import client.main.ConnectionManager;
import client.structures.IKeyGenerator;
import shared.request.KeyVerificationRequest;
import shared.response.BooleanResponse;
import shared.response.IResponse;
import shared.response.SuccessfulResponse;
import shared.response.UnsuccessfulResponse;

public class KeyGenerationValidator {


    //KeyGenerationValidator nezvysuje zadnou velikost delky dat apod.
    //zkusime pouze x krat vygenerovat klice a zkontrolovat je na serveru

    //pracujeme se dvema hlavnimi objekty -> connection manager a key generator
    //na kg delegujeme generovani
    //pres cm overujeme validitu
    private ConnectionManager cm;
    private IKeyGenerator kg;


    //pocet testu
    private static int TEST_COUNT = 10;

    protected KeyGenerationValidator(ConnectionManager cm, IKeyGenerator kg) {
        this.cm = cm;
        this.kg = kg;
    }

    public boolean validate() {
        return check();
    }


    //smycka dle poctu testu
    //drzime se dle rozvrzeni metod ostatnich validatoru
    private boolean check() {
        for(int i = 0; i < TEST_COUNT; i++) {
            if(!_check()) {
                return false;
            }
        }

        return true;
    }

    //samotne overeni jednoho vygenerovani
    private boolean _check() {
        //vygenerovani
        kg.generate();

        //odesleme vygenerovane klice na server, obdrzime response
        KeyVerificationRequest request = new KeyVerificationRequest(kg.getPrivateKey(), kg.getPublicKey(), kg.getKeyType());
        IResponse response = this.cm.sendRequest(request);

        //pro unsuccessfulresponse a booleanresponse s false vratime test unsuccessful

        if(response instanceof UnsuccessfulResponse) {
            System.err.println("Test unsuccessful");
            return false;
        }

        if (((BooleanResponse)response).getData()) {
            System.out.println("Test successful");
            return true;
        } else {
            System.err.println("Test unsuccessful");
            return false;
        }
    }
}
