package client.validation;

import client.main.ConnectionManager;
import client.structures.*;

//validator je do velke miry experiment
//neni idealne dekomponovan
// pokud by se mechanismus osvedcil, davalo by smysl ho vhodne dekomponovat a zobecnit funkcionalitu
public class Validator {

    //validator pro pripojeni vyuziva tridu connection manager
    private ConnectionManager connectionManager;

    public Validator(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;

        //pokud je connection manager neprihlaseny pri inicializaci validatoru, zahlasime do konzole warning
        if(!connectionManager.isLogged()) {
            System.out.println("Warning. ConnectionManager is not logged.");
        }
    }


    //metody maji dva kroky
    public boolean validate(ISymmetricCipher cipher) {

        //1. overeni, ze connection manager je prihlaseny
        //tentokrat nepustime dal, stejne by to selhalo
        if(!connectionManager.isLogged()) {
            System.out.println("Connection manager must be logged in.");
            return false;
        }

        //2. pokud je prihlaseny, tak delegujeme na konkretni tridu
        SymmetricCipherValidator val = new SymmetricCipherValidator(this.connectionManager, cipher);
        return val.validate();

        //ostatni metody jsou stejne
    }

    public boolean validate(IAsymmetricCipher cipher) {
        if(!connectionManager.isLogged()) {
            System.out.println("Connection manager must be logged in.");
            return false;
        }

        AsymmetricCipherValidator val = new AsymmetricCipherValidator(this.connectionManager, cipher);
        return val.validate();
    }

    public boolean validate(IHash hash) {
        if(!connectionManager.isLogged()) {
            System.out.println("Connection manager must be logged in.");
            return false;
        }

        HashValidator val = new HashValidator(this.connectionManager, hash);
        return val.validate();
    }

    public boolean validate(ISignature signature) {
        if(!connectionManager.isLogged()) {
            System.out.println("Connection manager must be logged in.");
            return false;
        }

        SignatureValidator val = new SignatureValidator(this.connectionManager, signature);
        return val.validate();
    }

    public boolean validate(IKeyGenerator keyGenerator) {
        if(!connectionManager.isLogged()) {
            System.out.println("Connection manager must be logged in.");
            return false;
        }

        KeyGenerationValidator val = new KeyGenerationValidator(this.connectionManager, keyGenerator);
        return val.validate();
    }

}
