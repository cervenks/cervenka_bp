package client.validation;

import client.main.ConnectionManager;
import client.structures.IAsymmetricCipher;
import shared.request.*;
import shared.response.DataResponse;
import shared.response.IResponse;
import shared.response.SuccessfulResponse;
import shared.response.UnsuccessfulResponse;

import javax.xml.bind.DatatypeConverter;
import java.security.KeyPair;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class AsymmetricCipherValidator {

    //pracujeme se dvema hlavnimi objekty -> connection manager a cipher
    //na ty deleguje sifrovani
    private ConnectionManager connectionManager;
    private IAsymmetricCipher cipher;

    //vygenerovane klice se ulozi sem
    private byte[] publicKey;
    private byte[] privateKey;

    //toto urcuje, s jakymy daty budeme zkouset algoritmus
    //zacneme na FIRST_BYTE_SIZE, ktery vzdy nasobime MULTIPLIER,
    // dokud delka nepresahne MAX_BYTES_SIZE (viz metoda check())
    private static final int MAX_BYTES_SIZE = 128;
    private static final int FIRST_BYTE_SIZE = 1;
    private static final int MULTIPLIER = 2;

    //protected, protoze ho chceme volat pouze pres validator
    protected AsymmetricCipherValidator(ConnectionManager connectionManager, IAsymmetricCipher cipher) {
       //jednoducha delegace
        this.connectionManager = connectionManager;
        this.cipher = cipher;

        //vygenerovani klicu, vyuzivame KeyGenerator
        byte[][] keys = KeyGenerator.getAsymmetricKeys(this.cipher.getCipherAlgorithm());
        assert keys != null;

        //klice nastavime pro tuto tridu i pro sifru
        this.privateKey = keys[0];
        this.publicKey = keys[1];
        this.cipher.setPrivateKey(this.privateKey);
        this.cipher.setPublicKey(this.publicKey);
    }


    //verejna trida na validaci
    public boolean validate() {
        //nejdrive zkusim nahrat klic
        if(!uploadKey()) {
            //pripadny vypis do konzole o neuspechu probehne uz v ramci metody uploadKey
            return false;
        }


        //pote se presuneme na samotne testovani
        return check();
    }

    private boolean uploadKey() {
        //posleme request, prijmeme response
        IRequest request = new KeyUploadRequest(this.publicKey, cipher.getCipherAlgorithm().getServerKey());
        IResponse response = connectionManager.sendRequest(request);

        //testovani neuspechu
        if(response instanceof UnsuccessfulResponse) {
            //vypis o neuspechu
            System.err.println("No success. Reason: " + ((UnsuccessfulResponse)response).getReason());
            return false;
        }

        return true;
    }

    //kontrola, vsech x pokusu
    private boolean check() {

        //tady ulozena soucasna delka testovanych dat
        //napoprve FIRST_BYTE_SIZE
        //pote vzdy vynasobime konstantou MULTIPLIER
        int currentBytesSize = FIRST_BYTE_SIZE;


        //pokud soucasna delka bude vetsi nez MAX_BYTES_SIZE, ukoncime
        while(currentBytesSize <= MAX_BYTES_SIZE) {

            //v _check() je kontrola jednoho otestovani
            if(!_check(currentBytesSize)) {
                return false;
            }

            currentBytesSize *= MULTIPLIER;
        }

        //pokud vsechny testy zahlasily true, dostane se program az sem
        return true;
    }


    //kontrola jednoho plaintextu
    private boolean _check(int size) {

        //vygenerovani nahodneho plaintextu dle delky
        byte[] data = new byte[size];
        ThreadLocalRandom.current().nextBytes(data);

        //posleme request, obdrzime response
        IResponse response = connectionManager.sendRequest(new CipherRequest(data, this.cipher.getCipherAlgorithm(), BlockCipherMode.ECB));

        //kontrola na unsuccsful_response
        if(response instanceof UnsuccessfulResponse) {
            System.err.println("No success. Reason: " +  ((UnsuccessfulResponse)response).getReason());
            return false;
        }

        //melo by byt zajisteno dle protokolu
        assert response instanceof DataResponse;


        //ziskani vysledku ze serveru a z lokalni sifry
        byte[] serverResult = ((DataResponse)response).getData();
        byte[] localResult = this.cipher.encryptWithPublicKey(data);


        //porovnani
        if(Arrays.equals(serverResult, localResult)) {
            System.out.println("Encrypted data " + size + " bytes long same as server data.");
        } else {
            System.err.println("Encryption failed. Data " + size + " bytes long.");
            System.err.println("Data: " + DatatypeConverter.printHexBinary(data));
            System.err.println("Expected: " + DatatypeConverter.printHexBinary(serverResult));
            System.err.println("Actual: " + DatatypeConverter.printHexBinary(localResult));
            return false;
        }


        //u asymetrickych sifer zkusime jeste desifrovat a porovnat s puvodnimi daty
        byte[] decryptResult = this.cipher.decryptWithPrivateKey(serverResult);
        decryptResult = SignatureCipherAlgorithm.transform(decryptResult, size);


        //porovnani 2
        if(Arrays.equals(data, decryptResult)) {
            System.out.println("Decrypted data " + size + " bytes long same as original data.");
        } else {
            System.err.println("Decryption failed. Data " + size + " bytes long.");
            System.err.println("Cipher: " + DatatypeConverter.printHexBinary(localResult));
            System.err.println("Expected: " + DatatypeConverter.printHexBinary(data));
            System.err.println("Actual: " + DatatypeConverter.printHexBinary(decryptResult));
            return false;
        }

        //pokud se vse povedlo, dostane se program az sem
        return true;
    }
}

