package client.validation;

import Test.Tutorial.KnapsackAlgorithm;
import shared.request.CipherAlgorithm;
import shared.request.KeyType;
import shared.request.SignatureCipherAlgorithm;

import java.security.*;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

//generator klicu pro validator
public class KeyGenerator {

    public static byte[] generateSymmetricKey(CipherAlgorithm cipherAlgorithm) {
        byte[] rvalue;

        //pro caesara vygenerujeme specialni klic
        //pro ostatni v case vytvorime pole dat a nahrneme tam nahodna data
        switch(cipherAlgorithm) {
            case AES:
                rvalue = new byte[32];
                break;
            case DES:
                rvalue = new byte[8];
                break;
            case TRIPLE_DES:
                rvalue = new byte[24];
                break;
            case BLOWFISH:
                rvalue = new byte[56];
                break;
            case CAESAR:
                byte offset = (byte)ThreadLocalRandom.current().nextInt(26);
                rvalue = new byte[1];
                rvalue[0] = offset;
                return rvalue;
            default:
                return null;
        }

        ThreadLocalRandom.current().nextBytes(rvalue);

        return rvalue;

    }



    public static byte[][] getAsymmetricKeys(CipherAlgorithm cipherAlgorithm) {
        //0 -> PRIVATE KEY
        //1 -> PUBLIC KEY
        byte[][] rvalue = new byte[2][];


        //pro RSA i knapsack vlastni algoritmy
        switch(cipherAlgorithm) {
            case RSA:
                try {
                    KeyPairGenerator kpg;
                    kpg = KeyPairGenerator.getInstance("RSA");
                    kpg.initialize(2048);
                    KeyPair kp =  kpg.generateKeyPair();
                    rvalue[0] = kp.getPrivate().getEncoded();
                    rvalue[1] = kp.getPublic().getEncoded();
                } catch (NoSuchAlgorithmException e) {
                    //nestane se, RSA algoritmus zna
                    e.printStackTrace();
                    return null;
                }
                break;
            case KNAPSACK:
                KnapsackAlgorithm ka = new KnapsackAlgorithm();
                ka.generateKeys();
                rvalue[0] = ka.getPrivateKey();
                rvalue[1] = ka.getPublicKey();
                break;
            default:
                return null;
        }


        return rvalue;


    }


    //delegace na asymmetrickeys
    public static byte[][] getSignatureKeys(SignatureCipherAlgorithm  signatureCipherAlgorithm) {
        CipherAlgorithm cipherAlgorithm = signatureCipherAlgorithm.getCipherAlgorithm();
        assert cipherAlgorithm != null;
        return getAsymmetricKeys(cipherAlgorithm);
    }
}
