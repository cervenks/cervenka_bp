package client.structures;

import shared.request.BlockCipherMode;
import shared.request.CipherAlgorithm;

public interface ISymmetricCipher {

        CipherAlgorithm getCipherAlgorithm();
        BlockCipherMode getBlockCipherMode();

        byte[] encrypt(byte[] data);

        void setKey(byte[] key);
        void setInitVector(byte[] vector);

}
