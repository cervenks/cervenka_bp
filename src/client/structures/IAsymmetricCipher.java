package client.structures;

import shared.request.CipherAlgorithm;

public interface IAsymmetricCipher {

    CipherAlgorithm getCipherAlgorithm();

    byte[] encryptWithPublicKey(byte[] data);
    byte[] decryptWithPrivateKey(byte[] data);

    void setPublicKey(byte[] key);
    void setPrivateKey(byte[] key);

}
