package client.structures;

import shared.request.HashAlgorithm;
import shared.request.SignatureCipherAlgorithm;

public interface ISignature {

    HashAlgorithm getHashAlgorithm();
    SignatureCipherAlgorithm getCipherAlgorithm();
    byte[] sign(byte[] data);

    void setPrivateKey(byte[] key);



}
