package client.structures;

import shared.request.HashAlgorithm;

public interface IHash {

    HashAlgorithm getHashAlgorithm();
    byte[] hash(byte[] data);
}
