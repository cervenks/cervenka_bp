package client.structures;

import shared.request.KeyVerificationType;

public interface IKeyGenerator {

    KeyVerificationType getKeyType();
    void generate();
    byte[] getPrivateKey();
    byte[] getPublicKey();
}
