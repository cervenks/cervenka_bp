package client.cipher;

import client.structures.ISymmetricCipher;
import shared.request.BlockCipherMode;
import shared.request.CipherAlgorithm;

public class AdapterDES implements ISymmetricCipher {

    //priklad pouziti adapteru
    //delegujeme vse na tridu DES a doplnime meta informace o cipherAlgorithmu a blockCipherModu
    //jedna se o ECB, takze metoda setInitVector se ve validatoru ani nezavola


    private DES des;

    public AdapterDES() {
        this.des = new DES();
        des.generate_keys();
    }

    @Override
    public CipherAlgorithm getCipherAlgorithm() {
        return CipherAlgorithm.DES;
    }

    @Override
    public BlockCipherMode getBlockCipherMode() {
        return BlockCipherMode.ECB;
    }

    @Override
    public byte[] encrypt(byte[] data) {
        return des.encrypt(data);
    }


    @Override
    public void setInitVector(byte[] vector) {

    }

    @Override
    public void setKey(byte[] key) {
        des.set_key(key);
    }
}
