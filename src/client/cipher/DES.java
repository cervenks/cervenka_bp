package client.cipher;

import shared.request.CipherAlgorithm;
import shared.utils.Utils;

import java.nio.ByteBuffer;
import java.util.Random;

//neni komentovano, sifra je implementovana dle standardu, neocekavame zadne rozsirovani apod.
public class DES {

    private byte[] key;



    private final static int[] PC1 =
            {
                    57, 49, 41, 33, 25, 17,  9,
                    1, 58, 50, 42, 34, 26, 18,
                    10,  2, 59, 51, 43, 35, 27,
                    19, 11,  3, 60, 52, 44, 36,
                    63, 55, 47, 39, 31, 23, 15,
                    7, 62, 54, 46, 38, 30, 22,
                    14,  6, 61, 53, 45, 37, 29,
                    21, 13,  5, 28, 20, 12,  4
            };

    private final static int[] KEY_SHIFTS =
            {
                    1,  1,  2,  2,  2,  2,  2,  2,  1,  2,  2,  2,  2,  2,  2,  1
            };

    private final static int[] PC2 =
            {
                    14, 17, 11, 24,  1,  5,
                    3, 28, 15,  6, 21, 10,
                    23, 19, 12,  4, 26,  8,
                    16,  7, 27, 20, 13,  2,
                    41, 52, 31, 37, 47, 55,
                    30, 40, 51, 45, 33, 48,
                    44, 49, 39, 56, 34, 53,
                    46, 42, 50, 36, 29, 32
            };

    private final static int[] IP =
            {
                    58, 50, 42, 34, 26, 18, 10 , 2,
                    60, 52, 44, 36, 28, 20, 12, 4,
                    62, 54, 46, 38, 30, 22, 14, 6,
                    64, 56, 48, 40, 32, 24, 16, 8,
                    57, 49, 41, 33, 25, 17, 9, 1,
                    59, 51, 43, 35, 27, 19, 11, 3,
                    61, 53, 45, 37, 29, 21, 13, 5,
                    63, 55, 47, 39, 31, 23, 15, 7
            };

    private final static int[] invertedIP =
            {
                    40, 8, 48, 16, 56, 24, 64, 32,
                    39, 7, 47, 15, 55, 23, 63, 31,
                    38, 6, 46, 14, 54, 22, 62, 30,
                    37, 5, 45, 13, 53, 21, 61, 29,
                    36, 4, 44, 12, 52, 20, 60, 28,
                    35, 3, 43 ,11, 51, 19, 59, 27,
                    34, 2, 42, 10, 50, 18, 58, 26,
                    33, 1, 41, 9, 49, 17, 57, 25
            };

    private final static int[] e =
            {
                    32,  1,  2,  3,  4,  5,
                    4,  5,  6,  7,  8,  9,
                    8,  9, 10, 11, 12, 13,
                    12, 13, 14, 15, 16, 17,
                    16, 17, 18, 19, 20, 21,
                    20, 21, 22, 23, 24, 25,
                    24, 25, 26, 27, 28, 29,
                    28, 29, 30, 31, 32,  1
            };

    private final static int[] pBoxForF =
            {
                    16,  7, 20, 21,
                    29, 12, 28, 17,
                    1, 15, 23, 26,
                    5, 18, 31, 10,
                    2,  8, 24, 14,
                    32, 27,  3,  9,
                    19, 13, 30,  6,
                    22, 11,  4, 25
            };

    private static int[][] s1 = {
            {14, 4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7},
            {0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11,  9,  5,  3,  8},
            {4, 1, 14,  8, 13,  6, 2, 11, 15, 12,  9,  7,  3, 10,  5,  0},
            {15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}
    };

    private static int[][] s2 = {
            {15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10},
            {3, 13,  4, 7, 15,  2,  8, 14, 12,  0, 1, 10,  6,  9, 11,  5},
            {0, 14, 7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15},
            {13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14,  9}
    };

    private static int[][] s3 = {
            {10, 0, 9, 14, 6, 3, 15, 5,  1, 13, 12, 7, 11, 4, 2,  8},
            {13, 7, 0, 9, 3,  4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1},
            {13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14,  7},
            {1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}
    };

    private static int[][] s4 = {
            {7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15},
            {13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14,  9},
            {10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4},
            {3, 15, 0, 6, 10, 1, 13, 8, 9,  4, 5, 11, 12, 7, 2, 14}
    };

    private static int[][] s5 = {
            {2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9},
            {14, 11, 2, 12,  4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6},
            {4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14},
            {11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}
    };

    private static int[][] s6 = {
            {12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11},
            {10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8},
            {9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6},
            {4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}
    };

    private static int[][] s7 = {
            {4, 11, 2, 14, 15,  0, 8, 13 , 3, 12, 9 , 7,  5, 10, 6, 1},
            {13 , 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6},
            {1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2},
            {6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}
    };

    private static int[][] s8 = {
            {13, 2, 8,  4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7},
            {1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6 ,11, 0, 14, 9, 2},
            {7, 11, 4, 1, 9, 12, 14, 2,  0, 6, 10 ,13, 15, 3, 5, 8},
            {2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6 ,11}
    };

    private final static int[][][] sBox = {s1, s2, s3, s4, s5, s6, s7, s8};

    private byte[][] subKeys;


    public byte[][] getSubKeys() {
        return this.subKeys;
    }

    public void set_key(byte[] key) {
        this.key = key;
        this.subKeys = new byte[16][6];

        byte[] reducedKey = permute(key, 7, PC1);

        byte[] C0 = new byte[] {0,0,0,0};
        byte[] D0 = new byte[] {0,0,0,0};

        for(int i = 0; i < 28; i++) {
            int bit1 = Utils.getBitOfByteArray(reducedKey,i);
            int bit2 = Utils.getBitOfByteArray(reducedKey, 28 + i);
            Utils.setBitOfByteArray(C0, i, bit1);
            Utils.setBitOfByteArray(D0, i, bit2);
        }

        for(int i = 0; i < 16; i++) {
            C0 = shift(C0, 28, KEY_SHIFTS[i]);
            D0 = shift(D0, 28, KEY_SHIFTS[i]);

            byte[] subKey = new byte[7];
            for(int j = 0; j < 28; j++) {
                int bit1 = Utils.getBitOfByteArray(C0,j);
                int bit2 = Utils.getBitOfByteArray(D0,j);
                Utils.setBitOfByteArray(subKey, j, bit1);
                Utils.setBitOfByteArray(subKey, j + 28, bit2);
            }

            subKey = permute(subKey,6,PC2);
            this.subKeys[i] = subKey;
        }
    }

    public byte[] generate_keys() {
        Random rand = new Random();
        byte[] key = new byte[8];
        rand.nextBytes(key);

        set_key(key);

        return key;
    }



    public byte[] encrypt(byte[] data) {
       byte[][] blocks = splitBlocks(data);
       byte[][] encryptedBlocks = new byte[blocks.length][8];

       for(int i = 0; i < blocks.length; i++) {
           encryptedBlocks[i] = encryptBlock(blocks[i],false);
       }

       byte[] rvalue = new byte[encryptedBlocks.length * 8];

       for(int i = 0; i < blocks.length; i++) {
           for(int j = 0; j < 8; j++) {
               rvalue[i * 8 + j] = encryptedBlocks[i][j];
           }
       }



       return rvalue;
    }

    private byte[] shift(byte[] input, int length, int shift) {
        byte[] rvalue = new byte[(length - 1) / 8 + 1];

        for(int i = shift; i < length; i++) {
            int bit = Utils.getBitOfByteArray(input, i);
            Utils.setBitOfByteArray(rvalue, i - shift, bit);
        }
        for(int i = 0; i < shift; i++) {
            int bit = Utils.getBitOfByteArray(input, i);
            Utils.setBitOfByteArray(rvalue,length - shift + i, bit);
        }

        return rvalue;
    }


    private byte[][] splitBlocks(byte[] data) {

        int append = (8 - (data.length % 8)) % 8; //8 - zbytek. / se musi prepsat na 0, proto jeste jednou % 8
        ByteBuffer bb = ByteBuffer.allocate(data.length + append);
        for(int i = 0; i < data.length; i++) {
            bb.put(data[i]);
        }
        for(int i = 0; i < append; i++) {
            bb.put((byte)0);
        }

        bb.rewind();

        byte[][] rvalue = new byte[(data.length + append) / 8][8];

        for(int i = 0; i < rvalue.length; i++) {
            for(int j = 0; j < rvalue[i].length; j++) {
                rvalue[i][j] = bb.get();
            }
        }

        return rvalue;
    }

    private byte[] encryptBlock(byte[] block, boolean decrypt) {
        if(block.length != 8) {
            System.out.println("Blok nema 64 bitu");
            return null;
        }

        block = permute(block, IP);

        byte[] left = new byte[4];
        byte[] right = new byte[4];

        for(int i = 0; i < 4; i++) {
            left[i] = block[i];
            right[i] = block[i+4];
        }

        byte[][] subKeys = new byte[16][8];

        if(decrypt) {
            for(int i = 0; i < 16; i++) {
                subKeys[i] = this.subKeys[15 - i];
            }
        } else {
            subKeys = this.subKeys;
        }

        for(int i = 0; i < 16; i++) {
            byte[] fResult = f(right, subKeys[i]);
            byte[] xorResult = xor(left, fResult);
            left = right;
            right = xorResult;
        }

        byte[] rvalue = new byte[8];

        for(int i = 0; i < 4; i++) {
            rvalue[i] = right[i];
            rvalue[i + 4] = left[i];
        }

        rvalue = permute(rvalue,invertedIP);

        return rvalue;
    }


    private byte[] permute(byte[] input, int outputLength, int[] positions) {
        byte[] rvalue = new byte[outputLength];

        for(int i = 0; i < positions.length; i++) {
            int bit = Utils.getBitOfByteArray(input, positions[i] - 1);
            Utils.setBitOfByteArray(rvalue, i, bit);
        }

        return rvalue;
    }

    private byte[] permute(byte[] input, int[] positions) {
        return permute(input,input.length,positions);
   }


    private byte[] f(byte[] message, byte[] key) {
       byte[] expandedMessage = new byte[6];

       for(int i = 0; i < 48; i++) {
           int bit = Utils.getBitOfByteArray(message, e[i] - 1);
           Utils.setBitOfByteArray(expandedMessage, i, bit);
       }

       byte[] xorResult = xor(key, expandedMessage);
       byte[] rvalue = new byte[]{0,0,0,0};

       for(int i = 0; i < 8; i++) {
           int[] bits = new int[6];
           for(int j = 0; j < 6; j++) {
               bits[j] = Utils.getBitOfByteArray(xorResult, i * 6 + j);
           }

           int first = bits[0] * 2 + bits[5];
           int second = bits[1] * 8 + bits[2] * 4 + bits[3] * 2 + bits[4];

           int sBoxResult = sBox[i][first][second];
           byte[] sBoxResultByte = new byte[]{(byte)sBoxResult};

           for(int j = 0; j < 4; j++) {
               int bit = Utils.getBitOfByteArray(sBoxResultByte,4+j);
               Utils.setBitOfByteArray(rvalue,i*4 + j, bit);
           }
       }

       rvalue = permute(rvalue, pBoxForF);

       return rvalue;
   }


    private byte[] xor(byte[] first, byte[] second) {
        int itLength = Math.min(first.length, second.length);
        int outLength = Math.max(first.length, second.length);

        byte[] rvalue = new byte[outLength];


        for(int i = 0; i < itLength; i++) {
            rvalue[i] = (byte)(first[i] ^ second[i]);
        }

        for(int i = itLength; i < outLength; i++) {
            rvalue[i] = first.length > second.length ? first[i] : second[i];
        }

        return rvalue;






    }


    public byte[] getKey() {
        return key;
    }
}
