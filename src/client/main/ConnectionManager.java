package client.main;

import shared.request.IRequest;
import shared.request.LoginRequest;
import shared.response.IResponse;
import shared.response.Reason;
import shared.response.SuccessfulResponse;
import shared.response.UnsuccessfulResponse;

import java.io.IOException;
import java.util.Random;


//nastavba nad clientem
public class ConnectionManager {

    private String id;
    private Client client; //delegace nekterych funkci na clienta
    private final static String defaultIP = "127.0.0.1";
    private final static int defaultPort = 6666;

    //po pripojeni na server se hned zkusi prave jednou pripojit
    //pokud se uspesne pripoji, nastavi se promenna logged na true
    //doporucuji pri debugu nebo pri tvorbe trid kontrolovat tuto promennou
    //validator napr. zahlasi warning, pokud je mu pridan nepripojeny clientmanager
    private boolean logged = false;


    //4 konstruktory
    //kombinace uvedeni jmena a neuvedeni + uvedeni ip a port a neuvedeni
    //po pripojeni hned pokus o login

    public ConnectionManager() throws IOException {
        client = new Client(defaultIP, defaultPort);
        login(setupID());
    }

    public ConnectionManager(String nickname) throws IOException {
        client = new Client(defaultIP, defaultPort);
        login(nickname);
    }

    public ConnectionManager(String ip, int port) throws IOException {
        client = new Client(ip, port);
        login(setupID());
    }

    public ConnectionManager(String ip, int port, String nickname) throws IOException {
        client = new Client(ip, port);
        login(nickname);
    }



    //odpojeni, pouze delegace na clienta
    public void logout() throws IOException {
        client.stopConnection();
    }


    //pripojeni
    private void login(String id) {
        this.id = id;

        //vytvoreni login request dle id
        LoginRequest loginRequest = new LoginRequest(id);
        try {

            //odeslani
            IResponse response = client.sendObject(loginRequest);

            //pokud uspesne, tak logged true, jinak vypiseme vyjimky a logged zustava na false
            if(response instanceof SuccessfulResponse) {
                this.logged = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalResponseException e) {
            e.printStackTrace();
        }
    }


    //generovani nahodneho jmena
    private String setupID() {
        int leftLimit = '0';
        int rightLimit = 'z';
        int length = 8;
        Random random = new Random();


        //bereme 8 nahodnych znaku od 0 do z a vynechavame znaky 9-A a Z-a
        return  random.ints(leftLimit, rightLimit +1)
                .filter(i -> (i <= '9' || i >= 'A') && (i <= 'Z' || i >= 'a'))
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }


    //poslani requestu
    public IResponse sendRequest(IRequest request) {
        try {
            //delegace na clienta
            return this.client.sendObject(request);
        } catch (IOException | IllegalResponseException e) {
            e.printStackTrace();
        }

        //v pripade neuspechu prevedeme vyjimku na unsuccessful_response
        return new UnsuccessfulResponse(Reason.CONNECTION_ERROR);
    }


    //gettery
    public String getNickname() {
        return id;
    }

    public boolean isLogged() {
        return logged;
    }
}
