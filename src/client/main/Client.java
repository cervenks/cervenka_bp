package client.main;

import shared.request.IRequest;
import shared.response.IResponse;
import shared.response.SuccessfulResponse;

import java.io.*;
import java.net.Socket;


//trida client je nejnizsi vrstva pripojeni
//doporoceno misto toho pouzit ConnectionManager, ktera ma rozsirenou funkcionalitu
public class Client {



    private Socket clientSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;



    //konstruktor se hned pokusi pripojit
    public Client(String ip, int port) throws IOException {
        startConnection(ip, port);

    }

    //pokus o pripojeni
    //konstruktor new Socket se hned svaze se socketem na serveru
    private void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new ObjectOutputStream(clientSocket.getOutputStream());
        in = new ObjectInputStream(clientSocket.getInputStream());
    }


    //ukonceni pripojeni
    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }


    //poslani requestu
    //v pripade neuspechu hazime vyjimky
    public IResponse sendObject(IRequest request) throws IOException, IllegalResponseException {
        out.writeObject(request);

        Object obj = null;
        try {
            obj = in.readObject();
        } catch (ClassNotFoundException e) {
            throw new IllegalResponseException("Server has returned unknown class.");
        }

        if(obj instanceof IResponse) {
            return (IResponse) obj;
        } else {
            throw new IllegalResponseException("Server hasn't returned the Response object.");
        }
    }



}