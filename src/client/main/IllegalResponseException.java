package client.main;

public class IllegalResponseException extends Exception {

    public IllegalResponseException(String message) {
        super(message);
    }
}
