package shared.utils;

import shared.request.CipherAlgorithm;

import java.io.*;

public class KeyFile {

    private String path; //cesta k souboru
    private String title; //napriklad PUBLIC RSA, DES apod.
    private String encodedData; //data kodovana do stringu

    public KeyFile(String path, String title, String encodedData)  { //pripraveno pro volani write key
        this.path = path;
        this.title = title;
        this.encodedData = encodedData;
    }

    public KeyFile(String path, String title) { //pripraveno pro volani read key
        this.path = path;
        this.title = title;
    }

    public boolean checkExistence() {
        File f = new File(path);
        return f.exists();
    }

    //vytvori prazdny soubor
    private void createFile() {
        File f = new File(path);
        File parent = f.getParentFile();
        if(!parent.exists() && !parent.mkdirs()) {
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }
        try {
            if(!f.exists() && !f.createNewFile()) {
                throw new IllegalStateException("Couldn't create file: " + f);
            }
        } catch (IOException e) {
            //Neni vytvorena slozka, coz by mel zajistit kod vyse
            e.printStackTrace();
        }
    }

    //zapise do souboru klic
    public void writeKey() throws IOException {
        createFile();
        Writer out;
        try {
           out = new FileWriter(new File(path));
        } catch (IOException e) {
            //Soubor neexistuje, to by mel zajistit kod vyse
            e.printStackTrace();
            return;
        }

        out.write("-----BEGIN " + title + " KEY-----\n");
        out.write(encodedData + "\n");
        out.write("-----END " + title + " KEY-----\n");
        out.close();
    }

    //nacte klic ze souboru
    public void readKey() throws IOException {
        File f = new File(path);
        StringBuilder keyBuilder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(f));

        boolean readLine = false;
        while(true) {
            String line = reader.readLine();
            if(line != null) {
                if(line.equals("-----BEGIN "+ title + " KEY-----")) {
                    readLine = true;
                } else if(line.equals("-----END "+ title + " KEY-----")) {
                    readLine = false;
                } else if(readLine) {
                    keyBuilder.append(line);
                }
            } else {
                break;
            }
        }

        this.encodedData = keyBuilder.toString();
    }


    //smaze soubor s klicem
    public boolean removeKey() {
        File f = new File(path);
        if(!f.exists()) {
            return true;
        }

        return f.delete();
    }


    public String getPath() {
        return path;
    }

    public String getTitle() {
        return title;
    }

    public String getEncodedData() {
        return encodedData;
    }
}
