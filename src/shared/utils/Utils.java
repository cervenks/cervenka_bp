package shared.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Utils {

    //ruzne prevody apod. vetsinou pro ucel vypisu do konzole
    //vetsina nevyuzivana

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static byte[] toByteArray(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        byte[] data = new byte[1024];

        int nRead = is.read(data);
        buffer.write(data, 0, nRead);

        return buffer.toByteArray();
    }


    public static String toBinaryString(byte[] bytes) {
        StringBuilder builder = new StringBuilder();

        for(byte b : bytes) {
            builder.append(String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0'));
            builder.append(" ");
        }

        return builder.toString();
    }

    public static String toAsciiString(byte[] bytes) {
        return new String(bytes);
    }


    public static String toHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 3];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 3] = HEX_ARRAY[v >>> 4];
            hexChars[j * 3 + 1] = HEX_ARRAY[v & 0x0F];
            hexChars[j * 3 + 2] = ' ';
        }
        return new String(hexChars);
    }

    public static String longToString(long key, int length) {
        String sKey = Long.toBinaryString(key);
        String temp = "";

        //metoda toBinaryString nevraci nuly na zacatku, ty ovsem  algoritmu maji vyznam
        //pridame je tedy rucne
        for(int i = sKey.length(); i < length; i++) {
            temp += "0";
        }

        sKey = temp + sKey;

        return sKey;
    }

    public static long binToLong(String key) {


        if(key.charAt(0) == '1' && key.length() == 64) {
            key = "-" + key.substring(1);
        }
        return Long.parseLong(key, 2);
    }

    public static int binToInt(String key) {
        return Integer.parseInt(key, 2);
    }

    public static String plaintextToBin(String plaintext) {
        byte[] bytes = plaintext.getBytes();
        StringBuilder binary = new StringBuilder();
        for (byte b : bytes) {
            int val = b;
            for (int i = 0; i < 8; i++) {
                if((val & 128) == 0) {
                    binary.append("0");
                } else {
                    binary.append("1");
                }
                val <<= 1;
            }
        }
        return binary.toString();
    }

    public static long plaintextToLong(String plaintext) {
        String bin = plaintextToBin(plaintext);
        return Long.parseLong(bin, 2);
    }

    public static String binToHexUnlimited(String bin) {
        if(bin.length() % 4 != 0) {
            System.out.println("Delka binu musi byt delitelna 4");
            return null;
        }

        String rvalue = "";
        String[] blocks = new String[bin.length() / 4];
        for(int i = 0; i < blocks.length; i++) {
            blocks[i] = "";
        }
        for(int i = 0; i < blocks.length * 4; i++) {
            blocks[i/4] += bin.charAt(i);
        }

        for(int i = 0; i < bin.length() / 4; i++) {
            int decimal = Integer.parseInt(blocks[i],2);
            rvalue += Integer.toString(decimal, 16);
        }

        return  rvalue;


    }

    public static String hexToBinUnlimited(String hex) {

        String rvalue = "";
        for(int i = 0; i < hex.length(); i++) {
            long decimal = Integer.parseInt("" + hex.charAt(i),16);
            rvalue += longToString(decimal, 4);
        }

        return  rvalue;


    }

    public static String hexToPlaintextUnlimited(String hex) {
        byte[] val = new byte[hex.length() / 2];
        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(hex.substring(index, index + 2), 16);
            val[i] = (byte) j;
        }

        return new String(val);
    }

    public static String binToPlaintextUnlimited(String bin) {
        String hex = binToHexUnlimited(bin);
        return hexToPlaintextUnlimited(hex);
    }

    public static String plaintextToHex(String plaintext) {
        String bin = plaintextToBin(plaintext);
        return binToHexUnlimited(bin);
    }

    public static byte[] toByteArray(int[] input) {
        byte[] rvalue = new byte[input.length * 4];

        for(int i = 0; i < input.length; i++) {
            rvalue[4 * i] = (byte)(input[i] >>> 24);
            rvalue[4 * i + 1] = (byte)(input[i] >>> 16);
            rvalue[4 * i + 2] = (byte)(input[i] >>> 8);
            rvalue[4 * i + 3] = (byte)(input[i]);
        }

        return rvalue;
    }

    public static void printByteArray(byte[] input, String title) {
        System.out.print(title + " = ");
        for(int i = 0; i < 64; i++) {
            int bit = getBitOfByteArray(input, i);
            System.out.print(bit);
        }
        System.out.println();
    }

    public static void setBitOfByteArray(byte[] input, int position, int bit) {

        int a = position / 8;
        int b = 7 - position % 8;

        if(bit == 0) {
            input[a] &= ~(1 << b);
        } else if (bit == 1) {
            input[a] |= 1 << b;
        }
    }

    public static int getBitOfByteArray(byte[] input, int position) {
        int a = position / 8;
        int b = position % 8;

        byte temp = (byte)((input[a] << b) & -128);

        if(temp == -128) {
            return 1;
        }

        if(temp == 0) {
            return 0;
        }

        return -1;
    }

    public static boolean isAsciiPrintable(char ch) {
        return ch >= 32 && ch < 127;
    }

    public static boolean isAsciiPrintable(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (!isAsciiPrintable(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /*public static int[] toIntArray(String binString) {

    }*/


    public static void printByteArray(byte[] array) {
        StringBuilder builder = new StringBuilder();

        builder.append("{");
        boolean carka = false;
        for(int i = 0; i < array.length; i++) {
            if(carka) {
                builder.append(", ");
            }
            carka = true;
            builder.append(array[i]);

        }

        builder.append("}");

        System.out.println(builder.toString());
    }



}
