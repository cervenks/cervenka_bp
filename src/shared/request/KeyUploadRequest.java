package shared.request;

public class KeyUploadRequest extends KeyRequest {

    //pozadavek na nahrani klice

    private byte[] key;


    public KeyUploadRequest(byte[] key, KeyType keyType) {
        this.key = key;
        this.keyType = keyType;
    }


    public byte[] getKey() {
        return key;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.UPLOAD_KEY;
    }
}
