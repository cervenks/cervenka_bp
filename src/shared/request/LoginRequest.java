package shared.request;

public class LoginRequest implements IRequest {

    //pozadavek na prihlaseni

    private String nickname;


    //parametr je nickname
    public LoginRequest(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.LOGIN;
    }

    public String getNickname() {
        return nickname;
    }
}
