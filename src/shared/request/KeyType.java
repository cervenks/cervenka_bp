package shared.request;

public enum KeyType {
    //typ klice

    DES, TRIPLE_DES, AES, BLOWFISH, RSA_PUBLIC, RSA_PRIVATE, DSA_PUBLIC, DSA_PRIVATE, ECDSA_PUBLIC, ECDSA_PRIVATE, CAESAR,
    KNAPSACK_PRIVATE, KNAPSACK_PUBLIC;

    //jmeno pro ulozeni do souboru
    public String getTitle() {
        switch(this) {
            case TRIPLE_DES:
                return "TRIPLE DES";
            case RSA_PUBLIC:
                return "RSA PUBLIC";
            case DSA_PUBLIC:
                return "DSA PUBLIC";
            case ECDSA_PUBLIC:
                return "ECDSA PUBLIC";
            case RSA_PRIVATE:
                return "RSA PRIVATE";
            case DSA_PRIVATE:
                return "DSA PRIVATE";
            case ECDSA_PRIVATE:
                return "ECDSA PRIVATE";
            case KNAPSACK_PRIVATE:
                return "KNAPSACK PRIVATE";
            case KNAPSACK_PUBLIC:
                return "KNAPSACK PUBLIC";
        }

        return super.toString();
    }


    //nazev souboru, do ktereho se bude klic ukladat
    public String getFilename() {
        switch(this) {
            case DES:
                return "des.key";
            case TRIPLE_DES:
                return "triple_des.key";
            case AES:
                return "aes.key";
            case BLOWFISH:
                return "blowfish.key";
            case RSA_PUBLIC:
                return "rsa_public.key";
            case DSA_PUBLIC:
                return "dsa_public.key";
            case ECDSA_PUBLIC:
                return "ecdsa_public.key";
            case RSA_PRIVATE:
                return "rsa_private.key";
            case DSA_PRIVATE:
                return "dsa_private.key";
            case ECDSA_PRIVATE:
                return "ecdsa_private.key";
            case CAESAR:
                return "caesar.key";
            case KNAPSACK_PRIVATE:
                return "knapsack_private.key";
            case KNAPSACK_PUBLIC:
                return "knapsack_public.key";
        }
        return super.toString();
    }

}
