package shared.request;

public enum RequestType {
    CIPHER, HASH, SIGNATURE, KEY_VERIFICATION, UPLOAD_KEY, DOWNLOAD_KEY, REMOVE_KEY, LOGIN;
}
