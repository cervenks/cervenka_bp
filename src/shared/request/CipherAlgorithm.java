package shared.request;

public enum CipherAlgorithm {
    AES, BLOWFISH, DES, TRIPLE_DES, RSA, CAESAR, KNAPSACK;


    //podle toho, co prijima trida Cipher
    public String toString() {
        if(this == CipherAlgorithm.TRIPLE_DES) {
            return "DESede";
        }

        if(this == CipherAlgorithm.BLOWFISH) {
            return "Blowfish";
        }

        return super.toString();
    }


    //delka bloku v bitech
    public int getBlockSize() {
        switch(this) {
            case RSA:
            case KNAPSACK:
                return 1;
            case AES: return 128;
            case BLOWFISH:
            case DES:
            case TRIPLE_DES:
                return 64;
            case CAESAR:
                return 8;


        }

        return 1;
    }


    //jaky pouzivame klic pro sifrovani
    public KeyType getServerKey() {
        switch(this) {
            case DES:
                return KeyType.DES;
            case AES:
                return KeyType.AES;
            case TRIPLE_DES:
                return KeyType.TRIPLE_DES;
            case BLOWFISH:
                return KeyType.BLOWFISH;
            case RSA:
                return KeyType.RSA_PUBLIC;
            case CAESAR:
                return KeyType.CAESAR;
            case KNAPSACK:
                return KeyType.KNAPSACK_PUBLIC;
        }

        return null;
    }
}
