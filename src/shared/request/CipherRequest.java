package shared.request;

public class CipherRequest extends DataRequest {

    //request na sifrovani

    private CipherAlgorithm algorithm;
    private BlockCipherMode blockCipherMode;

    //data z data request
    //pridavame algoritmus a mod operace
    public CipherRequest(byte[] data, CipherAlgorithm algorithm, BlockCipherMode blockCipherMode) {
        this.data = data;
        this.algorithm = algorithm;
        this.blockCipherMode = blockCipherMode;
    }

    public CipherAlgorithm getAlgorithm() {
        return algorithm;
    }

    public BlockCipherMode getBlockCipherMode() {
        return blockCipherMode;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.CIPHER;
    }
}
