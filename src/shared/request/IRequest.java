package shared.request;

import java.io.Serializable;

public interface IRequest extends Serializable {

     //rozhrani pro pozadavek

     RequestType getRequestType();
}
