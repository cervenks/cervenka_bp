package shared.request;

public class KeyDownloadRequest extends KeyRequest {

    //pozadavek na stazeni klice

    public KeyDownloadRequest(KeyType keyType) {
        this.keyType = keyType;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.DOWNLOAD_KEY;
    }
}
