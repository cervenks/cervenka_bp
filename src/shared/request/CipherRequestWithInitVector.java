package shared.request;

public class CipherRequestWithInitVector extends CipherRequest {

    private byte[] initVector;

    //data, algoritmus a blockCipherMode z rodice
    //pridavame inicializacni vektor
    public CipherRequestWithInitVector(byte[] initVector, byte[] data, CipherAlgorithm algorithm, BlockCipherMode blockCipherMode) {
        super(data, algorithm, blockCipherMode);
        this.initVector = initVector;
    }

    public byte[] getInitVector() {
        return initVector;
    }
}
