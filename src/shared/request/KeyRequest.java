package shared.request;

public abstract class KeyRequest implements IRequest {

    //abstraktni trida pro pozadavky na manipulaci s klici

    protected KeyType keyType;

    @Override
    public abstract RequestType getRequestType();

    public KeyType getKeyType() {
        return keyType;
    }

}
