package shared.request;

public enum KeyVerificationType {
    RSA, KNAPSACK;
}
