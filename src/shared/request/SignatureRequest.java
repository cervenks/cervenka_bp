package shared.request;

public class SignatureRequest extends DataRequest {

    //pozadavek na overeni digitalniho podpisu

    private SignatureCipherAlgorithm cipherAlgorithm;
    private HashAlgorithm hashAlgorithm;
    private byte[] hash;

    public SignatureRequest(byte[] data, byte[] hash, SignatureCipherAlgorithm cipherAlgorithm, HashAlgorithm hashAlgorithm) {
        this.cipherAlgorithm = cipherAlgorithm;
        this.hashAlgorithm = hashAlgorithm;
        this.hash = hash;
        this.data = data;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.SIGNATURE;
    }

    public SignatureCipherAlgorithm getCipherAlgorithm() {
        return cipherAlgorithm;
    }

    public HashAlgorithm getHashAlgorithm() {
        return hashAlgorithm;
    }

    public byte[] getHash() {
        return hash;
    }
}
