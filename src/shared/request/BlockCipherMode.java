package shared.request;

public enum BlockCipherMode {
    CBC, CFB, CTR, CTS, ECB, OFB, PCBC;
}
