package shared.request;

public class KeyRemoveRequest extends KeyRequest {

    //pozadavek na odstraneni klice

    public KeyRemoveRequest(KeyType keyType) {
        this.keyType = keyType;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.REMOVE_KEY;
    }
}
