package shared.request;

public class KeyVerificationRequest implements IRequest {

    //pozadavek na overeni validity klicu


    //parametry jsou oba klice a jaky algoritmus overujeme
    private byte[] dataPrivateKey;
    private byte[] dataPublicKey;
    private KeyVerificationType keyVerificationType;

    public KeyVerificationRequest(byte[] dataPrivateKey, byte[] dataPublicKey, KeyVerificationType type) {
        this.dataPrivateKey = dataPrivateKey;
        this.dataPublicKey = dataPublicKey;
        this.keyVerificationType = type;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.KEY_VERIFICATION;
    }

    public byte[] getDataPrivateKey() {
        return dataPrivateKey;
    }

    public byte[] getDataPublicKey() {
        return dataPublicKey;
    }

    public KeyVerificationType getKeyVerificationType() {
        return keyVerificationType;
    }
}
