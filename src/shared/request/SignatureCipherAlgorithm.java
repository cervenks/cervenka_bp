package shared.request;

public enum SignatureCipherAlgorithm {

    //algoritmus pro overeni digitalniho podpisu (sifrovaci cast)

    RSA;


    public KeyType getServerKey() {
        switch(this) {
            case RSA:
                return KeyType.RSA_PUBLIC;
        }

        return null;
    }

    public String getKeyName() {
        return this.toString();

    }

    public CipherAlgorithm getCipherAlgorithm() {
        switch (this) {
            case RSA:
                return CipherAlgorithm.RSA;
            default:
                return null;
        }
    }


    //delka po desifrovani RSA nemusi odpovidat delce pred zasifrovanim
    //proto pripadne doplnime nuly na zacatek, aby delka odpovidala
    public static byte[] transform(byte[] data, int length) {
        byte[] rvalue = new byte[length];

        for(int i = 0; i < length; i++) {
            rvalue[i] = data[data.length - length + i];
        }

        return rvalue;
    }
}
