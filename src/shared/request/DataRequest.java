package shared.request;

public abstract class DataRequest implements IRequest {

    //request pracujici s daty

    protected byte[] data;

    public abstract RequestType getRequestType();

    public byte[] getData() {
        return this.data;
    }
}
