package shared.request;

import java.io.Serializable;

public class HashRequest extends DataRequest {

    //request na hash

    private HashAlgorithm algorithm;

    //data z rodice
    //pridava algoritmus
    public HashRequest(byte[] data, HashAlgorithm algorithm) {
        this.algorithm = algorithm;
        this.data = data;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.HASH;
    }

    public HashAlgorithm getAlgorithm() {
        return algorithm;
    }
}
