package shared.request;

public enum HashAlgorithm {
    MD2, MD5, SHA1, SHA256, SHA384, SHA512;


    //nazev pro tridu MessageDigest
    public String getMDTitle() {
        if(this == SHA1) {
            return "SHA-1";
        }

        if(this == SHA256) {
            return "SHA-256";
        }

        if(this == SHA384) {
            return "SHA-384";
        }

        if(this == SHA512) {
            return "SHA-512";
        }

        return super.toString();
    }

    //delka hashe v bitech
    public int getHashSize() {
        switch(this) {
            case MD2:
            case MD5:
                return 128;
            case SHA1:
                return 160;
            case SHA256:
                return 256;
            case SHA384:
                return 384;
            case SHA512:
                return 512;
            default:
                return -1;
        }
    }


    //nazev pro tridu signature
    //nakonec nevyuzito
    public String getSignatureTitle() {
        return toString();
    }
}
