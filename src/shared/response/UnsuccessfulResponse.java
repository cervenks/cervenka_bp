package shared.response;

public class UnsuccessfulResponse implements IResponse {

     //neuspesna odpoved s uvedenym duvodem

    private Reason reason;

    public UnsuccessfulResponse(Reason reason) {
        this.reason = reason;
    }

    @Override
    public boolean isSuccessful() {
        return false;
    }

    public Reason getReason() {
        return reason;
    }
}
