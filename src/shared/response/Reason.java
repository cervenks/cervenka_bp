package shared.response;

public enum Reason {

    //duvody neuspechu

    INVALID_OR_MISSING_KEY_FILE, //nelze precist klic ze souboru
    ILLEGAL_BLOCK_SIZE, //u blokovych sifer bez paddingu neodpovida delka bloku, v pripade RSA je delka zpravy moc dlouha
    ILLEGAL_RSA_PARAMETER, // v pripadech, kdy do RSA uvadime blokovy rezim nez ECB
    INVALID_KEY, //pouzity spatny klic (napr u RSA)
    ALGORITHM_ERROR, //chyba algoritmu, v pripadech, kdy uvedeme napr spatny nazev algoritmu apod., tato chyba indikuje chybu v kodu na serveru
    MISSING_INITIAL_VECTOR, //do blokovych modu, ktere vyzaduji inicializacni vektor, tento vektor neuvedeme
    REDUNDANT_INITIAL_VECTOR, //v pripade, kdy do modu ECB pridavame navic inicializacni vektor

    UNKNOWN_OBJECT_TYPE, //pokud server dostane jiny objekt nez IRequest
    LOGIN_FIRST, //pozadavek neprihlaseneho uzivatele
    ALREADY_LOGGED_IN, //pozadavek na prihlaseni prihlaseneho uzivatele
    LOGIN_NOT_PRINTABLE, //nickname obsahuje netisknutelne znaky
    NICKNAME_ALREADY_ON_SERVER, //uzivatel s danym jmenem uz je na serveru pripojeny
    CANNOT_WRITE_FILE, //chyba pri zalozeni nebo psani do souboru (s klicema)
    CANNOT_DELETE_FILE, //chyba pri mazani souboru (s klicema)

    CONNECTION_ERROR, //vypadek spojeni


}
