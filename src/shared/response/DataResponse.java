package shared.response;

public class DataResponse extends SuccessfulResponse {


    //uspesna odpoved s daty

    public DataResponse(byte[] data) {
        this.data = data;
    }

    private byte[] data;

    public byte[] getData() {
        return data;
    }
}
