package shared.response;

import java.io.Serializable;

public interface IResponse extends Serializable {

    boolean isSuccessful();
}
