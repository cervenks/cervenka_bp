package shared.response;

public class BooleanResponse extends SuccessfulResponse {

    //uspesna odpoved s hodnotou true/false

    private boolean data;

    public BooleanResponse(boolean data) {
        this.data = data;
    }



    public boolean getData() {
        return data;
    }
}
