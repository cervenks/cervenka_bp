package server.request;

import server.decrypt.cipher.ICipher;
import server.decrypt.hash.IHash;
import server.decrypt.key_verification.IKeyVerifier;
import server.decrypt.signature.ISignature;
import shared.request.*;
import shared.response.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;


//hlavni trida pro zpracovani requestu
public class RequestExecutor {

    public static IResponse execute(IRequest request, String clientName) {
        IResponse response;

        //rozvetveni dle typu requestu, kazdy request vlastni metoda

        if(request instanceof CipherRequest) {
            response = executeCipher((CipherRequest) request, clientName);
        } else if(request instanceof HashRequest) {
            response = executeHash((HashRequest) request);
        } else if(request instanceof SignatureRequest) {
            response = executeSignature((SignatureRequest) request, clientName);
        } else if(request instanceof KeyUploadRequest) {
            response = executeUploadKey((KeyUploadRequest) request, clientName);
        } else if(request instanceof KeyDownloadRequest) {
            response = executeDownloadKey((KeyDownloadRequest) request, clientName);
        } else if(request instanceof KeyRemoveRequest) {
            response = executeRemoveKey((KeyRemoveRequest)request, clientName);
        } else if (request instanceof KeyVerificationRequest) {
            response = executeKeyVerification((KeyVerificationRequest)request);
        } else if(request instanceof LoginRequest) {
            response = new UnsuccessfulResponse(Reason.ALREADY_LOGGED_IN);
        }  else {
            response = new UnsuccessfulResponse(Reason.ALGORITHM_ERROR);
        }

        return response;
    }


    //cipher jeste rozdelime na ecb a zbytek a overime, ze sedi init vector s modem operace
    private static IResponse executeCipher(CipherRequest request, String clientName)  {
        if(request instanceof CipherRequestWithInitVector &&
                request.getBlockCipherMode() == BlockCipherMode.ECB) {
            //init vector a ecb nejde
            return new UnsuccessfulResponse(Reason.REDUNDANT_INITIAL_VECTOR);
        }

        if(!(request instanceof  CipherRequestWithInitVector) &&
                request.getBlockCipherMode() != BlockCipherMode.ECB) {
            //tady rezim sifer chce vecotr, ale v request neni
            return new UnsuccessfulResponse(Reason.MISSING_INITIAL_VECTOR);
        }


        //tady uz vse sedi, takze delegujeme dal
        if(request instanceof CipherRequestWithInitVector) {
            return executeCipherWithInitVector((CipherRequestWithInitVector)request, clientName);
        } else {
            return executeCipherWithoutInitVector(request, clientName);
        }

    }

    //zpracovani sifrovaciho requestu s init vectorem
    private static IResponse executeCipherWithInitVector(CipherRequestWithInitVector request, String clientName) {
        IResponse response;
        //ziskame konkretni instaci sifry
        ICipher cipher = RequestTypeGetter.getCipher(request.getAlgorithm(), clientName);
        byte[] rvalue;
        assert cipher != null;


        try {
            //zkusime zasifrovat
            rvalue = cipher.executeWithVector(request.getBlockCipherMode(), request.getData(), request.getInitVector());

            //uspesne, vracime data response
            response = new DataResponse(rvalue);

            //zbytek je prevod vyjimky na unsuccessful response
        } catch (InvalidKeyException e) {
            response = new UnsuccessfulResponse(Reason.INVALID_KEY);
        } catch (IllegalBlockSizeException e) {
            response = new UnsuccessfulResponse(Reason.ILLEGAL_BLOCK_SIZE);
        } catch (BadPaddingException | InternalCipherException e) {
            response = new UnsuccessfulResponse(Reason.ALGORITHM_ERROR);
        } catch (IllegalParameterException e) {
            response = new UnsuccessfulResponse(Reason.ILLEGAL_RSA_PARAMETER);
        } catch (MissingOrInvalidKeyFileException e) {

            response = new UnsuccessfulResponse(Reason.INVALID_OR_MISSING_KEY_FILE);
        } catch (IllegalInitVectorException e) {
            response = new UnsuccessfulResponse(Reason.REDUNDANT_INITIAL_VECTOR);
        }

        return response;
    }


    //sifra bez init vectoru
    private static IResponse executeCipherWithoutInitVector(CipherRequest request, String clientName) {
        IResponse response;

        //ziskani konkretni sifrovaci instance
        ICipher cipher = RequestTypeGetter.getCipher(request.getAlgorithm(), clientName);
        assert cipher != null;

        byte[] rvalue = new byte[0];
        try {

            //zkusime zasifrovat
            rvalue = cipher.executeWithoutVector(request.getBlockCipherMode(), request.getData());

            //uspesne, vracime data response
            response = new DataResponse(rvalue);

            //zbytek je prevod vyjimky na unsuccessful response
        } catch (BadPaddingException | InternalCipherException e) {
            response = new UnsuccessfulResponse(Reason.ALGORITHM_ERROR);
        } catch (InvalidKeyException | InvalidKeySpecException e) {
            response = new UnsuccessfulResponse(Reason.INVALID_KEY);
        } catch (IllegalBlockSizeException e) {
            response = new UnsuccessfulResponse(Reason.ILLEGAL_BLOCK_SIZE);
        } catch (MissingOrInvalidKeyFileException e) {
            response = new UnsuccessfulResponse(Reason.INVALID_OR_MISSING_KEY_FILE);
        } catch (IllegalParameterException e) {
            response = new UnsuccessfulResponse(Reason.ILLEGAL_RSA_PARAMETER);
        }
        return response;
    }


    //zpracovani hashe
    private static IResponse executeHash(HashRequest request) {

        IResponse response;

        //ziskani instance
        IHash hash = RequestTypeGetter.getHash(request.getAlgorithm());
        assert hash != null;

        try {
            //zkusime zahashovat
            byte[] rvalue = hash.execute(request.getData());
            //uspech
            response = new DataResponse(rvalue);

            //neuspech
        } catch (Exception e) {
            response = new UnsuccessfulResponse(Reason.ALGORITHM_ERROR);
        }

        return response;
    }


    //zpracovani digitalniho podpisu
    private static IResponse executeSignature(SignatureRequest request, String clientName) {
        IResponse response;

        //ziskani instance
        ISignature signature = RequestTypeGetter.getSignature(request.getCipherAlgorithm(), request.getHashAlgorithm(), clientName);
        assert signature != null;

        boolean verified = false;
        try {

            //zkusime overit
            verified = signature.verify(request.getData(), request.getHash());

            //overeni uspesne
            response = new BooleanResponse(verified);

            //neuspech
        } catch (MissingOrInvalidKeyFileException e) {
            response = new UnsuccessfulResponse(Reason.INVALID_OR_MISSING_KEY_FILE);
        } catch (InternalCipherException e) {
            response = new UnsuccessfulResponse(Reason.ALGORITHM_ERROR);
        } catch (InvalidKeyException e) {
            response = new UnsuccessfulResponse(Reason.INVALID_KEY);
        }


        return response;
    }


    //pozadavek na nahrani klice
    private static IResponse executeUploadKey(KeyUploadRequest request, String clientName) {
        IResponse response;
        KeyType keyType = request.getKeyType();
        byte[] data = request.getKey();

        try{

            //delegace na tridu key manager
            KeyManager.setKey(clientName, keyType, data);
            response = new SuccessfulResponse();
        } catch (IOException e) {
            response = new UnsuccessfulResponse(Reason.CANNOT_WRITE_FILE);
        }

        return response;
    }


    //pozadavek na stahnuti klice
    private static IResponse executeDownloadKey(KeyDownloadRequest request, String clientName) {
        IResponse response;
        KeyType keyType = request.getKeyType();

        try {
            //delegace na tridu key manager
            byte[] data = KeyManager.getKey(clientName, keyType);
            response = new DataResponse(data);
        } catch (MissingOrInvalidKeyFileException e) {
            response = new UnsuccessfulResponse(Reason.CANNOT_WRITE_FILE);
        }
        return response;
    }


    //pozadavek na odstraneni klice
    private static IResponse executeRemoveKey(KeyRemoveRequest request, String clientName) {
        IResponse response;

        //delegace na tridu key manager
        if(KeyManager.removeKey(clientName, request.getKeyType())) {
            response = new SuccessfulResponse();
        } else {
            response = new UnsuccessfulResponse(Reason.CANNOT_DELETE_FILE);
        }

        return response;

    }

    //pozadavek na overeni klicu
    private static IResponse executeKeyVerification(KeyVerificationRequest request) {
        IResponse response;
        KeyVerificationType type = request.getKeyVerificationType();

        //ziskani instance
        IKeyVerifier verifier = RequestTypeGetter.getKeyVerifier(type);
        assert verifier != null;


        try {

            //zkusime osetrit
            boolean bool = verifier.verify(request.getDataPublicKey(), request.getDataPrivateKey());

            //proces probehl uspesne
            response = new BooleanResponse(bool);

            //neuspech
        } catch (InvalidKeyException | InvalidKeySpecException e) {
            response = new UnsuccessfulResponse(Reason.INVALID_KEY);
        } catch (InternalCipherException e) {
            response = new UnsuccessfulResponse(Reason.ALGORITHM_ERROR);
        }


        return response;


    }
}
