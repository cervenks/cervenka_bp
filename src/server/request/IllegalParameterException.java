package server.request;

public class IllegalParameterException extends Exception {
    public IllegalParameterException(String message) {
        super(message);
    }
}
