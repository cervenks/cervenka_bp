package server.request;

public class InternalCipherException extends Exception {
    public InternalCipherException(String errMsg) {
        super(errMsg);
    }
}
