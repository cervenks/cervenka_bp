package server.request;

public class MissingOrInvalidKeyFileException extends Exception {
    public MissingOrInvalidKeyFileException(String errMsg) {
        super(errMsg);
    }
}
