package server.request;

public class IllegalInitVectorException extends Exception  {
    public IllegalInitVectorException(String errorMsg) {
        super(errorMsg);
    }
}
