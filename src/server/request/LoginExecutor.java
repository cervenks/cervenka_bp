package server.request;

import server.main.Server;
import shared.request.LoginRequest;
import shared.response.IResponse;
import shared.response.Reason;
import shared.response.SuccessfulResponse;
import shared.response.UnsuccessfulResponse;

public class LoginExecutor {

    public static IResponse executeLogin(LoginRequest request, Server.ClientHandling handling, Server server) {
        //toto je login, ktery kontrolujeme
        String login = request.getNickname();

        //overeni tisknutelnych znaku
        if(!isAsciiPrintable(login)) {
            return new UnsuccessfulResponse(Reason.LOGIN_NOT_PRINTABLE);
        }

        //overeni, jestli uz na serveru neni
        if(server.isLogged(login)) {
            return new UnsuccessfulResponse(Reason.NICKNAME_ALREADY_ON_SERVER);
        }

        //pokud proslo, tak nastavime login a posleme success
        handling.setClientId(login);
        return new SuccessfulResponse();
    }


    private static boolean isAsciiPrintable(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();

        //ziskame delku a kontrolujeme po znaku
        for (int i = 0; i < sz; i++) {
            if (isAsciiPrintable(str.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

    //printable znaky jsou v ascii tabulce v danem intervalu
    private static boolean isAsciiPrintable(char ch) {
        return ch >= 32 && ch < 127;
    }
}
