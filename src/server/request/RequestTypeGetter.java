package server.request;

import server.decrypt.cipher.*;
import server.decrypt.hash.IHash;
import server.decrypt.hash.JavaHash;
import server.decrypt.key_verification.IKeyVerifier;
import server.decrypt.key_verification.KnapsackVerifier;
import server.decrypt.key_verification.RSAVerifier;
import server.decrypt.signature.ISignature;
import server.decrypt.signature.RSAManualSignature;
import shared.request.CipherAlgorithm;
import shared.request.HashAlgorithm;
import shared.request.KeyVerificationType;
import shared.request.SignatureCipherAlgorithm;

public class RequestTypeGetter {

    //trida se stara o
    //prevod algoritmu na konkretni tridu


    static ICipher getCipher(CipherAlgorithm algorithm, String clientName) {
        switch(algorithm) {
            case DES:
            case AES:
            case TRIPLE_DES:
            case BLOWFISH:
                return new JavaBlockCipher(algorithm, clientName);
            case RSA:
                return new JavaRSA(clientName);
            case CAESAR:
                return new CaesarCipher(clientName);
            case KNAPSACK:
                return new KnapsackCipher(clientName);
            default:
                return null;
        }
    }

    static IHash getHash(HashAlgorithm algorithm) {
        switch(algorithm) {
            case SHA1:
            case SHA256:
            case SHA384:
            case SHA512:
            case MD2:
            case MD5:
                return new JavaHash(algorithm);

            default:
                return null;
        }
    }

    static ISignature getSignature(SignatureCipherAlgorithm cipherAlgorithm, HashAlgorithm hashAlgorithm, String clientName) {
        switch(cipherAlgorithm) {
            case RSA:
                return new RSAManualSignature(cipherAlgorithm, hashAlgorithm, clientName);
            default:
                return null;
        }
    }

    static IKeyVerifier getKeyVerifier(KeyVerificationType type) {
        switch(type) {
            case RSA:
                return new RSAVerifier();
            case KNAPSACK:
                return new KnapsackVerifier();
        }

        return null;
    }


}
