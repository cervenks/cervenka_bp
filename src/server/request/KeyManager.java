package server.request;

import server.main.Server;
import shared.request.KeyType;
import shared.utils.KeyFile;

import java.io.IOException;
import java.util.Base64;

public class KeyManager {

    //ziskani klice
    public static byte[] getKey(String clientName, KeyType keyType) throws MissingOrInvalidKeyFileException {
        String path = Server.pathToAllFolders + "/" + clientName + "/" + keyType.getFilename();
        String title = keyType.getTitle();

        //vytvoreni key file a delegace na key file viz bakalarska prace
        KeyFile keyFile = new KeyFile(path, title);
        try {
            keyFile.readKey();
        } catch (IOException e) {
            throw new MissingOrInvalidKeyFileException("Missing or invalid key.");
        }
        return Base64.getDecoder().decode(keyFile.getEncodedData());

    }

    //nastaveni klice
    public static void setKey(String clientName, KeyType keyType, byte[] data) throws IOException {
        String path = Server.pathToAllFolders + "/" + clientName + "/" + keyType.getFilename();
        String title = keyType.getTitle();

        //vytvoreni key file a delegace na key file viz bakalarska prace
        KeyFile keyFile = new KeyFile(path, title, Base64.getEncoder().encodeToString(data));
        keyFile.writeKey();
    }


    //odstraneni klice
    public static boolean removeKey(String clientName, KeyType keyType) {
        String path = Server.pathToAllFolders + "/" + clientName + "/" + keyType.getFilename();
        String title = keyType.getTitle();


        //vytvoreni key file a delegace na key file viz bakalarska prace
        KeyFile keyFile = new KeyFile(path, title);
        return keyFile.removeKey();
    }



}
