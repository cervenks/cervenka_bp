package server.request;

public class MissingInitVectorException extends Exception {
    public MissingInitVectorException(String errMsg) {
        super(errMsg);
    }
}
