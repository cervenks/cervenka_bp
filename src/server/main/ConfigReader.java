package server.main;

import java.util.Properties;
import java.io.*;


public class ConfigReader {

    //tabulka typu klic hodnota
    private Properties properties;

    public ConfigReader(String filename) {
        this.properties = new Properties();
        InputStream is = null;

        try {
            is = new FileInputStream(filename);
            properties.load(is); //nacteme tabulku ze souboru
        } catch (FileNotFoundException e) {
            System.err.println("File named " + filename + " not found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //ziskame string z tabulky
    public String getString(String key) {
        return properties.getProperty(key);
    }

    //ziskame int z tabulky
    public int getInt(String key) {
        String val = properties.getProperty(key);
        return Integer.parseInt(val);
    }






}
