package server.main;

import server.request.LoginExecutor;
import server.request.RequestExecutor;
import shared.request.IRequest;
import shared.request.LoginRequest;
import shared.response.IResponse;
import shared.response.Reason;
import shared.response.UnsuccessfulResponse;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class Server {
    private Logger logger; //pro logovani
    private ConfigReader reader; //pro konfigurace

    public static File pathToAllFolders; //cesta ke slozce s klici

    private List<ClientHandling> users = new ArrayList<>(); //seznam uzivatelu

    private int maxClientCount; //z konfigu
    private int port; //z konfigu

    public Server(ConfigReader reader) throws IOException {
        this.reader = reader;
        pathToAllFolders = new File(reader.getString("path_to_keys"));

        setupLogger();
    }


    //inicializujeme logger
    private void setupLogger() throws IOException {
        this.logger = Logger.getLogger("ServerLog");

        String logFileName = reader.getString("log_file_name");
        FileHandler handler = new FileHandler(logFileName, true);
        this.logger.addHandler(handler);

        SimpleFormatter formatter = new SimpleFormatter();
        handler.setFormatter(formatter);
    }



    private void start() throws IOException {

        //nacteni z configu
        maxClientCount = getReader().getInt("max_client_count");
        port = getReader().getInt("port");

        getLogger().info("Server starts on port " + port + ".");
        System.out.println("Server.Main.Server is running");

        try (ServerSocket listener = new ServerSocket(port)) {
            //executor service -> rozsireni pro vlakna, moznost nastavit max clienty
            ExecutorService pool = Executors.newFixedThreadPool(maxClientCount);
            while (true) {
                //vytvorime tridu clienta, ktera implementuje runnable
                ClientHandling handling = new ClientHandling(listener.accept(), this);
                users.add(handling);
                getLogger().info("User connected on port " + handling.socket.getPort() + ".");
                broadcastOnlineClients();

                //spustime vlakno
                pool.execute(handling);
            }
        }
    }


    //synchronizovane metody, ktere volaji jednotliva vlakna

    synchronized public boolean isLogged(String searchedNickname) {
        for (ClientHandling loggedUser : this.users) {
            if (searchedNickname.equals(loggedUser.getClientId())) {
                return true;
            }
        }
        return false;
    }


    synchronized private void removeClient(ClientHandling clientHandling) {
        this.users.remove(clientHandling);
    }

    synchronized private void logWarning(String message) {
        getLogger().warning(message);
    }

    synchronized private void logInfo(String message) {
        getLogger().info(message);
    }

    synchronized private void broadcastOnlineClients() {
        if (users.size() > maxClientCount) {
            getLogger().info("Online users: (" + users.size() + "/" + maxClientCount + "). Users in queue.");
        } else {
            getLogger().info("Online users: (" + users.size() + "/" + maxClientCount + ").");
        }

    }

    //vstupni metoda serveru
    public static void main(String[] args) throws IOException {
        //inicializujeme server, config, logger apod.
        Server server = new Server(new ConfigReader("config.properties"));
        server.start();
    }


    //gettery
    private Logger getLogger() {
        return this.logger;
    }

    private ConfigReader getReader() {
        return this.reader;
    }


    ///////////////////////////////////////////////////////////////////////
    //trida client handling reprezentujici jedno vlakno/klienta
    public static class ClientHandling implements Runnable {

        //odkazy na server a komunikaci
        private Socket socket;
        private Server server;
        private ObjectOutputStream out;
        private ObjectInputStream in;

        //jmeno clienta
        private String clientId = null;


        //odkaz na socket, server
        ClientHandling(Socket socket, Server server) {
            this.socket = socket;
            this.server = server;

        }

        @Override
        public void run() {
            System.out.println("Connected: " + socket);
            try {

                //hlavni funkcionalita vlakna, nacteme si proudy dat
                in = new ObjectInputStream(socket.getInputStream());
                out = new ObjectOutputStream(socket.getOutputStream());


                //spustime login loop
                doLoginLoop();
                //az login loop dobehne, prejde program do standard loop
                doStandardLoop();

            } catch (Exception e) {
                //vypadek clienta
                System.out.println("Error:" + socket);
                if (clientId == null) {
                    server.logWarning("Not logged user with port " + socket.getPort() + " lost connection.");
                } else {
                    server.logWarning("User " + clientId + " lost connection.");
                }

            } finally {
                //uzavreni socketu, odebrani clienta ze seznamu apod.
                try {
                    socket.close();
                } catch (IOException e) {
                }
                System.out.println("Closed: " + socket);

                this.server.removeClient(this);
                this.server.broadcastOnlineClients();

            }
        }

        //prijmuti request, obaleni object streamu
        private IRequest getRequest() throws IOException {
            //prijmeme objekt
            Object obj;
            try {
                obj = in.readObject();
            } catch (ClassNotFoundException e) {
                sendResponse(new UnsuccessfulResponse(Reason.UNKNOWN_OBJECT_TYPE));
                return null;
            }

            //zkontrolujeme, ze objekt je IRequest
            if (obj instanceof IRequest) {
                return (IRequest) obj;
            } else {
                sendResponse(new UnsuccessfulResponse(Reason.UNKNOWN_OBJECT_TYPE));
                return null;
            }

        }

        //odeslani, nemusime nic obalovat
        void sendResponse(IResponse obj) throws IOException {
            out.writeObject(obj);
        }


        //getter a setter na nickname
        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }


        //smycka cekajici na spravny login request
        private void doLoginLoop() throws IOException {
            while (true) {
                IRequest request = getRequest();

                //neplatny request, ignorujeme
                if (request == null) {
                    server.logWarning("Received invalid Request on port " + socket.getPort() + ". Expected Login Request.");
                    continue;
                }

                //obdrzen jiny request nez login, posilame unsuccessful response
                if (!(request instanceof LoginRequest)) {
                    server.logWarning("Received valid Request but expected Login Request on port " + socket.getPort() + ". Sending Unsuccessful Response.");
                    sendResponse(new UnsuccessfulResponse(Reason.LOGIN_FIRST));
                    continue;
                }

                //tady uz mame login request
                IResponse response = LoginExecutor.executeLogin((LoginRequest) request, this, this.server);

                //neco se nepovedlo (neplatny login)
                if (response instanceof UnsuccessfulResponse) {
                    server.logWarning("Received invalid Login Request. Sending Unsuccessful Response.");
                    sendResponse(response);
                    continue;
                }

                //spravny request
                server.logInfo("User " + clientId + " successfully logged in.");
                sendResponse(response);
                break;
            }
        }


        //smycka cekajici na requesty
        private void doStandardLoop() throws IOException {
            while (true) {
                IRequest request = getRequest();
                if (request == null) {
                    server.logWarning("Received invalid Request by user " + clientId + ".");
                    continue;
                }

                //zahlasime do logu a spustime
                server.logInfo(clientId + " sends new " + request.getClass().getName() + ".");
                IResponse response = RequestExecutor.execute(request, clientId);
                server.logInfo("Responding with " + response.getClass().getName() + " to " + clientId + ".");
                sendResponse(response);
            }
        }
    }
}