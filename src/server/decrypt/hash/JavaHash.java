package server.decrypt.hash;

import server.request.InternalCipherException;
import shared.request.HashAlgorithm;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class JavaHash implements IHash {

    private HashAlgorithm algorithm;


    //nacteme algoritmus
    public JavaHash(HashAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    @Override
    public byte[] execute(byte[] data) throws InternalCipherException {

        //delegujeme na tridu message digest
        String algorithmStr = algorithm.getMDTitle();
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(algorithmStr);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Defined bad enum algorithm name to String.");
            e.printStackTrace();
            throw new InternalCipherException("Defined bad enum algorithm name to String.");
        }


        return md.digest(data);
    }
}
