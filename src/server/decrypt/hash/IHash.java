package server.decrypt.hash;

import shared.request.HashAlgorithm;

public interface IHash {

    byte[] execute(byte[] data) throws Exception;
}
