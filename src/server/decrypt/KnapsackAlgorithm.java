package server.decrypt;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.concurrent.ThreadLocalRandom;

public class KnapsackAlgorithm {

    //komentare viz Test.Tutorial.KnapsackAlgorithm

    private static int NUMBER_SIZE_IN_BYTES = 2;


    private static int WEIGHT_COUNT = 8;

    private BigInteger[] privateKeyInt;
    private BigInteger[] publicKeyInt;
    private byte[] privateKeyBytes;
    private byte[] publicKeyBytes;

    private BigInteger q;
    private BigInteger p;


    public void generateKeys() {
        generatePrivateKey();
        generatePublicKey();
        convertKeys();

    }

    public byte[] getPrivateKey() {
        byte[] rvalue = new byte[privateKeyBytes.length + 2 * NUMBER_SIZE_IN_BYTES];
        ByteBuffer byteBuffer = ByteBuffer.wrap(rvalue);
        byteBuffer.put(bigIntegerToByteArray(q, NUMBER_SIZE_IN_BYTES));
        byteBuffer.put(bigIntegerToByteArray(p, NUMBER_SIZE_IN_BYTES));
        byteBuffer.put(privateKeyBytes);
        return rvalue;
    }

    public byte[] getPublicKey() {
        return publicKeyBytes;
    }

    public void setPrivateKey(byte[] privateKey) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(privateKey);
        byte[] qBytes = new byte[NUMBER_SIZE_IN_BYTES];
        byte[] pBytes = new byte[NUMBER_SIZE_IN_BYTES];
        byteBuffer.get(qBytes, 0, NUMBER_SIZE_IN_BYTES);
        byteBuffer.get(pBytes, 0, NUMBER_SIZE_IN_BYTES);

        this.q = new BigInteger(qBytes);
        this.p = new BigInteger(pBytes);


        this.privateKeyBytes = new byte[privateKey.length - (2 * NUMBER_SIZE_IN_BYTES)];
        byteBuffer.get(privateKeyBytes, 0, privateKey.length - (2 * NUMBER_SIZE_IN_BYTES));
        this.privateKeyInt = parseByteArrayToBigIntegerArray(this.privateKeyBytes);

    }

    public void setPublicKey(byte[] publicKey) {
        this.publicKeyBytes = publicKey;
        this.publicKeyInt = parseByteArrayToBigIntegerArray(this.publicKeyBytes);
    }

    private void generatePrivateKey() {
        int possibleSum = (int)Math.ceil(Math.log(WEIGHT_COUNT) / Math.log(2));
        int bitLength = NUMBER_SIZE_IN_BYTES * 8 - 1 - possibleSum - WEIGHT_COUNT;

        privateKeyInt = new BigInteger[WEIGHT_COUNT];

        BigInteger sum = BigInteger.ZERO;

        for(int i = 0; i < WEIGHT_COUNT; i++) {
            BigInteger random = new BigInteger(bitLength, ThreadLocalRandom.current()).add(BigInteger.ONE);
            BigInteger newNumber = sum.add(random);
            sum = sum.add(newNumber);
            privateKeyInt[i] = newNumber;
        }

        BigInteger random = new BigInteger(bitLength, ThreadLocalRandom.current()).add(BigInteger.ONE);
        q = sum.add(random);
        //p = q.subtract(BigInteger.ONE);

        while(true) {
            p = new BigInteger(q.bitLength(), ThreadLocalRandom.current());

            if(p.compareTo(q) >= 0) {
                continue;
            }

            try{
                p.modInverse(q);
            } catch (Exception e) {
                continue;
            }

            break;
        }

    }

    private void generatePublicKey() {
        publicKeyInt = new BigInteger[WEIGHT_COUNT];

        for(int i = 0; i < WEIGHT_COUNT; i++) {
            BigInteger number = privateKeyInt[i].multiply(p);
            number = number.mod(q);
            publicKeyInt[i] = number;
        }
    }

    private byte[] convertKey(BigInteger[] key) {
        byte[] rvalue = new byte[key.length * NUMBER_SIZE_IN_BYTES];
        ByteBuffer buffer = ByteBuffer.wrap(rvalue);

        for(int i = 0; i < key.length; i++) {
            buffer.put(bigIntegerToByteArray(key[i], NUMBER_SIZE_IN_BYTES));
        }

        return buffer.array();
    }

    private byte[] bigIntegerToByteArray(BigInteger bigInteger, int length) {
        byte[] array = bigInteger.toByteArray();
        byte[] tmp = new byte[length];
        int newPos = tmp.length - array.length;
        System.arraycopy(array, 0, tmp, newPos, array.length);
        return tmp;


    }

    private void convertKeys() {
        this.privateKeyBytes = convertKey(privateKeyInt);
        this.publicKeyBytes = convertKey(publicKeyInt);
    }

    public byte[] encrypt(byte[] data) {
        //pro jednoduchost ocekavame, ze delka zpravy bude delitelna poctem vah v zavazadlu
        //tzn. ze velikost bloku vyjde "celociselne!
        //defaultne je pocet vah 8, takze tato podminka je zarucena vzdy

        int resultSize = data.length * 8; //delka zpravy v bitech
        int blockCount = resultSize / WEIGHT_COUNT; // pocet bloku
        resultSize = blockCount * NUMBER_SIZE_IN_BYTES; // pocet bloku vynasobeno velikosti cisla reprezentujici blok

        byte[] rvalue = new byte[resultSize];
        ByteBuffer buffer = ByteBuffer.wrap(rvalue);

        for(int i = 0; i < blockCount; i++) {
            byte[] block = encryptBlock(data, i * WEIGHT_COUNT);
            buffer.put(block);
        }

        return buffer.array();
    }

    public byte[] decrypt(byte[] data) {

        BigInteger inverseP = p.modInverse(q);
        BigInteger[] numbers = parseByteArrayToBigIntegerArray(data);
        BigInteger[] decryptedNumbers = new BigInteger[numbers.length];

        for(int i = 0; i < numbers.length; i++) {
            BigInteger current = numbers[i].multiply(inverseP);
            current = current.mod(this.q);
            decryptedNumbers[i] = current;
        }

        int resultSize = data.length / NUMBER_SIZE_IN_BYTES; //pocet cisel ve zprave
        resultSize = resultSize * WEIGHT_COUNT; //delka zpravy v bitech
        resultSize = resultSize / 8; //delka zpravy v bytech
        byte[] rvalue = new byte[resultSize];


        for(int i = 0; i < decryptedNumbers.length; i++) {
            decryptBlock(rvalue, i * WEIGHT_COUNT, decryptedNumbers[i]);
        }


        return rvalue;

    }

    private BigInteger[] parseByteArrayToBigIntegerArray(byte[] data) {
        int numberCount = data.length / NUMBER_SIZE_IN_BYTES;
        BigInteger[] rvalue = new BigInteger[numberCount];
        ByteBuffer buffer = ByteBuffer.wrap(data);


        for(int i = 0; i < numberCount; i++) {
            byte[] current = new byte[NUMBER_SIZE_IN_BYTES];
            buffer.get(current, 0, NUMBER_SIZE_IN_BYTES);
            rvalue[i] = new BigInteger(current);
        }

        return rvalue;
    }

    private byte[] encryptBlock(byte[] data, int startPos) {
        BigInteger rvalue = BigInteger.ZERO;
        for(int i = 0; i < WEIGHT_COUNT; i++) {
            int bit = getBitOfByteArray(data,startPos + i);
            if(bit == 1) {
                rvalue = rvalue.add(this.publicKeyInt[i]);
            }
        }

        return bigIntegerToByteArray(rvalue, NUMBER_SIZE_IN_BYTES);
    }

    private void decryptBlock(byte[] rvalue, int startPos, BigInteger number) {
        for(int i = this.privateKeyInt.length - 1; i >= 0; i--) {
            if(number.compareTo(this.privateKeyInt[i]) >= 0) {
                setBitOfByteArray(rvalue, startPos + i, 1);
                number = number.subtract(this.privateKeyInt[i]);
            }
        }
    }



    private int getBitOfByteArray(byte[] input, int position) {
        int a = position / 8;
        int b = position % 8;

        byte temp = (byte)((input[a] << b) & -128);

        if(temp == -128) {
            return 1;
        }

        if(temp == 0) {
            return 0;
        }

        return -1;
    }

    public static void setBitOfByteArray(byte[] input, int position, int bit) {

        int a = position / 8;
        int b = 7 - position % 8;

        if(bit == 0) {
            input[a] &= ~(1 << b);
        } else if (bit == 1) {
            input[a] |= 1 << b;
        }
    }

}
