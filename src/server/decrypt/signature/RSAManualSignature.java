package server.decrypt.signature;

import server.request.InternalCipherException;
import server.request.KeyManager;
import server.request.MissingOrInvalidKeyFileException;
import shared.request.HashAlgorithm;
import shared.request.SignatureCipherAlgorithm;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

public class RSAManualSignature implements ISignature {

    private HashAlgorithm hashAlgorithm;
    private SignatureCipherAlgorithm cipherAlgorithm;
    private String clientName;


    //nacteme oba algoritmy a jmeno clienta
    public RSAManualSignature(SignatureCipherAlgorithm cipherAlgorithm, HashAlgorithm hashAlgorithm, String clientName) {
        this.hashAlgorithm = hashAlgorithm;
        this.cipherAlgorithm = cipherAlgorithm;
        this.clientName = clientName;
    }

    @Override
    public boolean verify(byte[] data, byte[] hashToVerify) throws MissingOrInvalidKeyFileException, InternalCipherException, InvalidKeyException {
        boolean rvalue;

        try {
            //oddeleni ochytani vyjimek a implementace kvuli prehlednosti
            rvalue = _verify(data, hashToVerify);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new InternalCipherException("No Such Algorithm Exception");
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new InternalCipherException("Illegal Block Size Exception");
        } catch (BadPaddingException e) {
            e.printStackTrace();
            throw new InternalCipherException("Bad Padding Exception");
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            throw new InternalCipherException("Invalid Key Spec Exception");
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            throw new InternalCipherException("No Such Padding Exception");
        }

        return rvalue;
    }

    private boolean _verify(byte[] data, byte[] hashToVerify) throws MissingOrInvalidKeyFileException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidKeySpecException, NoSuchPaddingException {

       //provedeme algoritmus overeni

        //hash ze zpravy
        byte[] hashFromData = hash(data);

        //desifrovany hash
        byte[] decryptedHash = cipher(hashToVerify);
        decryptedHash = SignatureCipherAlgorithm.transform(decryptedHash, this.hashAlgorithm.getHashSize() / 8);


        //porovnani
        return Arrays.equals(hashFromData, decryptedHash);
    }

    private byte[] hash(byte[] data) throws NoSuchAlgorithmException {
        //provedenei Message Digest hashe
        String algorithmStr = hashAlgorithm.getMDTitle();
        MessageDigest md = MessageDigest.getInstance(algorithmStr);
        return md.digest(data);
    }

    private byte[] cipher(byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException, MissingOrInvalidKeyFileException, InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        //musi zajistit vyssi vrstva
        assert cipherAlgorithm == SignatureCipherAlgorithm.RSA;
        //nestane se vzhledem k definici metody
        assert cipherAlgorithm.getServerKey() != null;


        //provedeni RSA zasifrovani
        byte[] key = KeyManager.getKey(clientName, cipherAlgorithm.getServerKey());
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(key);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey publicKey = kf.generatePublic(keySpec);

        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(data);

    }
}
