package server.decrypt.signature;

import server.request.InternalCipherException;
import server.request.MissingOrInvalidKeyFileException;
import shared.request.HashAlgorithm;
import shared.request.SignatureCipherAlgorithm;

import java.io.IOException;
import java.security.InvalidKeyException;

public interface ISignature {

    boolean verify(byte[] data, byte[] hashToVerify) throws MissingOrInvalidKeyFileException, InternalCipherException, InvalidKeyException;
}
