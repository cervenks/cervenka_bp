package server.decrypt.key_verification;

import server.request.InternalCipherException;

import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public interface IKeyVerifier {

    boolean verify(byte[] publicKey, byte[] privateKey)
            throws InvalidKeyException, //pokud selze neco uvnitr algoritmu, melo by byt odchyceno
            InternalCipherException, InvalidKeySpecException; //klic je nevalidni
}
