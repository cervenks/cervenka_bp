package server.decrypt.key_verification;

import server.request.InternalCipherException;
import shared.request.SignatureCipherAlgorithm;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RSAVerifier implements IKeyVerifier {

    private PrivateKey privateKey;
    private PublicKey publicKey;


    @Override
    public boolean verify(byte[] dataPublicKey, byte[] dataPrivateKey) throws InternalCipherException, InvalidKeyException, InvalidKeySpecException {

        boolean rvalue;

        try {
            //oddeleni kodu a chytani vyjimek kvuli prehlednosti
            rvalue = _verify(dataPublicKey, dataPrivateKey);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Invalid algorithm name.");
            e.printStackTrace();
            throw new InternalCipherException("Invalid algorithm name.");
        } catch (NoSuchPaddingException e) {
            System.err.println("Invalid padding.");
            e.printStackTrace();
            throw new InternalCipherException("Invalid padding.");
        } catch (BadPaddingException e) {
            System.err.println("Bad padding for decrypting.");
            e.printStackTrace();
            throw new InternalCipherException("Bad padding for decrypting.");
        } catch (IllegalBlockSizeException e) {
            System.err.println("Illegal block size.");
            e.printStackTrace();
            throw new InternalCipherException("Illegal block size.");
        }


        return rvalue;
    }

    private boolean _verify(byte[] dataPublicKey, byte[] dataPrivateKey) throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        //nastavime klice
        setPrivateKey(dataPrivateKey);
        setPublicKey(dataPublicKey);

        //vygenerujeme nahodny blok dat, proc delka 5 viz bakalarska prace
        byte[] challenge = new byte[5];
        ThreadLocalRandom.current().nextBytes(challenge);

        //ziskame instanci
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");


        //zasifrujeme
        cipher.init(Cipher.ENCRYPT_MODE, this.publicKey);
        byte[] result1 = cipher.doFinal(challenge);


        //desifrujeme a prodlouzime na puvodni delku dat
        cipher.init(Cipher.DECRYPT_MODE, this.privateKey);
        byte[] result2 = cipher.doFinal(result1);
        result2 = SignatureCipherAlgorithm.transform(result2, 5);


        //rovnaji se => vratime true
        //nerovnaji se => vratime false
        return Arrays.equals(result2, challenge);


    }


    private void setPrivateKey(byte[] data) throws InvalidKeySpecException, NoSuchAlgorithmException {
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(data);
        //RSA by to sice melo poznat vzdy, ale vyjimku propaguju na vyssi uroven,
        //protoze kdyby neco selhalo, tak at o tom vi hlavni metoda
        KeyFactory kf = KeyFactory.getInstance("RSA");

        this.privateKey = kf.generatePrivate(keySpec);
    }

    private void setPublicKey(byte[] data) throws NoSuchAlgorithmException, InvalidKeySpecException {
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(data);
        //RSA by to sice melo poznat vzdy, ale vyjimku propaguju na vyssi uroven,
        //protoze kdyby neco selhalo, tak at o tom vi hlavni metoda
        KeyFactory kf = KeyFactory.getInstance("RSA");

        this.publicKey = kf.generatePublic(keySpec);
    }
}
