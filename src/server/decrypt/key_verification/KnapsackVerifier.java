package server.decrypt.key_verification;


import server.decrypt.KnapsackAlgorithm;
import server.request.InternalCipherException;

import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class KnapsackVerifier implements IKeyVerifier {

    //podobne jako u KnapsackCipher, budeme delegovat na tridu KnapsackAlgoritm

    @Override
    public boolean verify(byte[] publicKey, byte[] privateKey) throws InvalidKeyException, InternalCipherException, InvalidKeySpecException {
        KnapsackAlgorithm ka = new KnapsackAlgorithm();

        //nacteme klice do knapsack

        try {
            ka.setPublicKey(publicKey);
            ka.setPrivateKey(privateKey);
        } catch (Exception e) {
            throw new InternalCipherException("Unable to load keys");
        }

        byte[] challenge = new byte[50];
        ThreadLocalRandom.current().nextBytes(challenge);


        //zasifrujeme a desifrujeme pomoci knapsack
        byte[] cipher = new byte[0];
        byte[] result = new byte[0];
        try {
            cipher = ka.encrypt(challenge);
            result = ka.decrypt(cipher);
        } catch (Exception e) {
            throw new InternalCipherException("Unable to encrypt or decrypt");
        }

        //rovnaji se => vratime true
        //nerovnaji se => vratime false
        return (Arrays.equals(challenge, result));
    }
}
