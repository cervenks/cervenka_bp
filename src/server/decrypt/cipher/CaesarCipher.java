package server.decrypt.cipher;

import server.request.*;
import shared.request.BlockCipherMode;
import shared.request.KeyType;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;

public class CaesarCipher implements ICipher {

    private String clientName;


    //konstruktor pracuje se jmenem klienta pro ziskani klicu
    public CaesarCipher(String clientName) {
        this.clientName = clientName;
    }


    //caesar nemuze byt spusten s init vectorem
    @Override
    public byte[] executeWithVector(BlockCipherMode mode, byte[] data, byte[] vector) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, MissingOrInvalidKeyFileException, IllegalParameterException, InternalCipherException, IllegalInitVectorException {
        throw new IllegalInitVectorException("Caesar can't be loaded with init vector");
    }



    @Override
    public byte[] executeWithoutVector(BlockCipherMode mode, byte[] data) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, MissingOrInvalidKeyFileException, IllegalParameterException, InternalCipherException, InvalidKeySpecException {
        //ziskani klice
        byte[] key = KeyManager.getKey(clientName, KeyType.CAESAR);

        //sifrovany text bude stejne dlouhy jako text prosty
        byte[] rvalue = new byte[data.length];

        //kazy znak posuneme
        for(int i = 0; i < data.length; i++) {
            char c = moveChar((char)data[i], key[0]);
            rvalue[i] = (byte)c;
        }

        return rvalue;

    }

    private char moveChar(char c, int offset) {
        if(!(between('A', 'Z', c))) { //budeme posouvat pismena pouze z rozsahu A-Z
            return c;
        }

        int originalPos = c - 'A'; //pozice v abecede
        int newPos = (originalPos + offset) % 26; //nova pozice v abecede

        return (char)('A' + newPos);
    }

    private boolean between(char min, char max, char c) {
        return c >= min && c <= max;
    }
}
