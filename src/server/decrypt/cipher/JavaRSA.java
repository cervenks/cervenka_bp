package server.decrypt.cipher;

import server.request.IllegalParameterException;
import server.request.InternalCipherException;
import server.request.KeyManager;
import server.request.MissingOrInvalidKeyFileException;
import shared.request.BlockCipherMode;
import shared.request.KeyType;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class JavaRSA implements ICipher {

    public static final String ALGORITHM_STR = "RSA";
    private String clientName;

    //nacteme jmeno klienta, algoritmus bude vzdy RSA
    public JavaRSA(String clientName) {
        this.clientName = clientName;
    }


    //RSA nelze spustit s init vectorem
    @Override
    public byte[] executeWithVector(BlockCipherMode mode,  byte[] data, byte[] vector) throws IllegalParameterException {
        throw new IllegalParameterException("RSA can't be loaded with initial vector.");
    }

    @Override
    public byte[] executeWithoutVector(BlockCipherMode mode, byte[] data) throws IllegalParameterException, MissingOrInvalidKeyFileException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InternalCipherException, InvalidKeySpecException {

        //RSA musi byt v ECB
        if(mode != BlockCipherMode.ECB) {
            throw new IllegalParameterException("RSA must be in ECB mode.");
        }



        byte[] rvalue = null;

        //ziskani klice
        byte[] key = KeyManager.getKey(clientName, KeyType.RSA_PUBLIC);


        Cipher cipher = null;
        try {
            //tady zkusime ziskat instanci
            cipher = Cipher.getInstance("RSA/ECB/NoPadding");

            //parametr getInstance nemenime, takze dle autora by se vyjimky stat nemely
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Invalid algorithm name.");
            e.printStackTrace();
            throw new InternalCipherException("Invalid algorithm name.");
        } catch (NoSuchPaddingException e) {
            System.err.println("Invalid padding.");
            e.printStackTrace();
            throw new InternalCipherException("Invalid padding.");
        }

        //nacteme klic do formatu pro Cipher
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(key);
        PublicKey publicKey = null;


        try {
            //nacteme klic do instance Cipher
            publicKey = KeyFactory.getInstance("RSA").generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            //RSA pozna tridu X509KeySpec, takze toto by se stal nemelo
            System.err.println("Invalid definition of key factory.");
            e.printStackTrace();
            throw new InternalCipherException("Invalid definition of key factory.");
        }

        //zasifrujeme
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        rvalue = cipher.doFinal(data);


        return rvalue;
    }
}
