package server.decrypt.cipher;

import server.decrypt.KnapsackAlgorithm;
import server.request.*;
import shared.request.BlockCipherMode;
import shared.request.KeyType;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;

public class KnapsackCipher implements ICipher {

    //budeme pouzivat tridu KnapsackAlgorithm, kde je zavazadlovy algoritmus implementovan
    //vse budeme delegovat tam

    private String clientName;
    private KnapsackAlgorithm algorithm;


    public KnapsackCipher(String clientName) {
        this.clientName = clientName;
    }


    //nelze s init vectorem
    @Override
    public byte[] executeWithVector(BlockCipherMode mode, byte[] data, byte[] vector) throws IllegalInitVectorException {
        throw new IllegalInitVectorException("Can't load Knapsack with init vector");
    }

    @Override
    public byte[] executeWithoutVector(BlockCipherMode mode, byte[] data) throws IllegalParameterException, MissingOrInvalidKeyFileException {

        //musi byt v ecb
        if (mode != BlockCipherMode.ECB) {
            throw new IllegalParameterException("Knapsack must be in ECB mode.");
        }

        //ziskame klic
        byte[] key = KeyManager.getKey(clientName, KeyType.KNAPSACK_PUBLIC);

        //deleguje na knapsack algorithm
        this.algorithm = new KnapsackAlgorithm();
        this.algorithm.setPublicKey(key);

        return this.algorithm.encrypt(data);


    }




}
