package server.decrypt.cipher;

import server.request.InternalCipherException;
import server.request.KeyManager;
import server.request.MissingOrInvalidKeyFileException;
import shared.request.BlockCipherMode;
import shared.request.CipherAlgorithm;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class JavaBlockCipher implements ICipher {

    private CipherAlgorithm cipherAlgorithm;
    private String clientName;


    //konstruktor pracuje s nazvem algoritmu, ktery preparsuje do tridy Cipher
    //a s jmenem klienta kvuli ziskani klice
    public JavaBlockCipher(CipherAlgorithm cipherAlgorithm, String clientName) {
        this.cipherAlgorithm = cipherAlgorithm;
        this.clientName = clientName;
    }

    @Override
    public byte[] executeWithVector(BlockCipherMode mode, byte[] data, byte[] vector) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, MissingOrInvalidKeyFileException, InternalCipherException {

        //to string vraci nazev presne pro tridu Cipher
        String modeStr = mode.toString();
        String paddingStr = "NoPadding";
        String algorithmStr = cipherAlgorithm.toString();


        byte[] rvalue = new byte[0];

        //nestane se, osestrene vsechny moznosti
        assert cipherAlgorithm.getServerKey() != null;

        //ziskani klice
        byte[] key = KeyManager.getKey(clientName, cipherAlgorithm.getServerKey());

        try {
            //zkusime zasifrovat a odchytame vyjimky
            rvalue = encryptWithVector(algorithmStr, modeStr, paddingStr, key, data, vector);

            //vyjimky, ktere nemuze zpusobit klient prevedeme do internal cipher exception
            //dle autora by nemelo nastat
        } catch (NoSuchPaddingException e) {
            System.err.println("Defined bad enum padding to String.");
            e.printStackTrace();
            throw new InternalCipherException("Defined bad enum padding to String.");
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Defined bad enum algorithm name to String.");
            e.printStackTrace();
            throw new InternalCipherException("Defined bad enum algorithm name to String.");
        } catch (InvalidAlgorithmParameterException e) {
            //Spatny inicializacni vektor
            e.printStackTrace();
            throw new InternalCipherException("Bad initial vector against policy.");
        }
        return rvalue;
    }

    @Override
    public byte[] executeWithoutVector(BlockCipherMode mode, byte[] data) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, MissingOrInvalidKeyFileException, InternalCipherException {

        //viz metoda vyse

        String modeStr = mode.toString();
        String paddingStr = "NoPadding";
        String algorithmStr = cipherAlgorithm.toString();
        byte[] rvalue = new byte[0];
        assert cipherAlgorithm.getServerKey() != null;
        byte[] key = KeyManager.getKey(clientName, cipherAlgorithm.getServerKey());

        try {
            rvalue = encryptWOVector(algorithmStr, modeStr, paddingStr, key, data);
        } catch (NoSuchPaddingException e) {
            //CHYBA V ENUM to String nebo parsovani algoritmu
            System.err.println("Defined bad enum padding to String.");
            e.printStackTrace();
            throw new InternalCipherException("Defined bad enum padding to String.");
        } catch (NoSuchAlgorithmException e) {
            //CHYBA V ENUM to String nebo parsovani algoritmu, ktere v teto metode nemaji co delat
            System.err.println("Defined bad enum algorithm name to String.");
            e.printStackTrace();
            throw new InternalCipherException("Defined bad enum algorithm name to String.");
        }

        return rvalue;
    }

    private byte[] encryptWithVector(String algorithm, String mode, String padding, byte[] key, byte[] data, byte[] vector) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        //tady probiha samotne sifrovani
        //vyjimky posilame o uroven vyse

        Cipher cipher = Cipher.getInstance(algorithm + "/" + mode + "/" + padding);
        SecretKeySpec keySpec = new SecretKeySpec(key, "" + algorithm);
        IvParameterSpec iv = new IvParameterSpec(vector);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);
        return cipher.doFinal(data);
    }

    private byte[] encryptWOVector(String algorithm, String mode, String padding, byte[] key, byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        //tady probiha samotne sifrovani
        //vyjimky posilame o uroven vyse

        Cipher cipher = Cipher.getInstance(algorithm + "/" + mode + "/" + padding);
        SecretKeySpec keySpec = new SecretKeySpec(key, "" + algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        return cipher.doFinal(data);
    }



}
