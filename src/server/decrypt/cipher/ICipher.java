package server.decrypt.cipher;

import server.request.IllegalInitVectorException;
import server.request.IllegalParameterException;
import server.request.InternalCipherException;
import server.request.MissingOrInvalidKeyFileException;
import shared.request.BlockCipherMode;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;


public interface ICipher {

    byte[] executeWithVector(BlockCipherMode mode, byte[] data, byte[] vector) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, MissingOrInvalidKeyFileException, IllegalParameterException, InternalCipherException, IllegalInitVectorException;
    byte[] executeWithoutVector(BlockCipherMode mode, byte[] data) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, MissingOrInvalidKeyFileException, IllegalParameterException, InternalCipherException, InvalidKeySpecException;
}
